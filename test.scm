(load "init.scm")
(load "assertions.scm")

;
; ---- TEST PREDICATES ----
;
 (display "  test predicates")(newline)

; --- boolean? ---

(ASSERT_TRUE
    (boolean? #t)
    "#t should be boolean")

(ASSERT_TRUE
    (boolean? #f)
    "#f should be boolean")

; --- void? ---

(ASSERT_TRUE
    (void? #void)
    "#void test failed")

(ASSERT_FALSE
    (void? #nil)
    "#nil should not be #void")

; --- nil? ---

(ASSERT_TRUE
    (nil? #nil)
    "#nil test failed")

(ASSERT_FALSE
    (nil? #void)
    "#void should not be #nil")

; --- number? ---

(ASSERT_TRUE
    (number? (+ 2 (- 10 8)))
    "expression should be evaluated to number")

(ASSERT_TRUE
    (number? (- 10))
    "-10 should be a number")

(ASSERT_FALSE
    (number? #t)
    "#t should not be a number")

(ASSERT_FALSE
    (number? #f)
    "#f should not be a number")

(ASSERT_FALSE
    (number? 'a)
    "symbol should not be a number")

(ASSERT_FALSE
    (number? "123")
    "number in string should not be a number")

; --- string? ---

(ASSERT_TRUE
    (string? "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.")
    "expression should be evaluated to string")

(ASSERT_FALSE
    (string? 1)
    "expression should be not evaluated to string")



;
; ---- TEST CONDITIONS ----
;
 (display "  test conditions")(newline)

(define (exec_cond param)
    (cond
            ((equal? param "Exit")   "Exit program")
            ((equal? param 0)        #f)
            ((equal? param 1)        #t)
            ((equal? param #t)       100)
            (else                    "Unknown command")
    )
)

(ASSERT_EQUAL
    (exec_cond "Exit")
    "Exit program"
    "First cond should be processed")

(ASSERT_FALSE
    (exec_cond (- 2 1 1))
    "Second cond should be processed")

(ASSERT_TRUE
    (exec_cond (+ 1 0))
    "Third cond should be processed")

(ASSERT_EQUAL
    (exec_cond (equal? "Hello" "Hello"))
    100
    "Fourth cond should be processed")

(ASSERT_EQUAL
    (exec_cond (- 1234))
    "Unknown command"
    "Else part should be processed")

(ASSERT_EQUAL
    (cond (#t "OK"))
    "OK"
    "Cond: should be OK string")

(ASSERT_EQUAL
    (cond (#f "OK"))
    #void
    "Cond: should return #void if not match")

(ASSERT_EQUAL
    (cond (#f "FAIL") (else "OK"))
    "OK"
    "Cond: should return else part")

(ASSERT_EQUAL
    (cond (#t "FAIL-1" "FAIL-2" "FAIL-3" "OK") (else "FAIL-ELSE-1" "FAIL-ELSE-2"))
    "OK"
    "Cond: should support multiple bodies")

;
; ---- TEST LET ----
;
;
;(define list-1 '(5 1 8 8 368 7 5 54 8 5 48 76 23 18 48 86 2 21 5 4))
;(define list-1-sorted '(1 2 4 5 5 5 5 7 8 8 8 18 21 23 48 48 54 76 86 368))
;
;
;(ASSERT_EQUAL (let ((a 3)) (! a)) (* 3 2 1) "Let test failed")
;(ASSERT_EQUAL
;    (let ((x 0) (y 1))
;        (let ((x y) (y x))
;            (list x y)))
;    '(1 0)
;    "Values are not independent of the variables and the order of evaluation")
;(ASSERT_EQUAL
;    (let ((x 10))
;        (+ x 3))
;    13
;    "Let test 3 failed")
;

;
; ---- TEST LIST OPS ----
;
 (display "  test list operations")(newline)

; prepare lists:
(define list-1 '(5 1 8 8 368 7 5 54 8 5 48 76 23 18 48 86 2 21 5 4))
(define list-1-sorted '(1 2 4 5 5 5 5 7 8 8 8 18 21 23 48 48 54 76 86 368))


(ASSERT_EQUAL
    (filter (lambda (x) (= (% x 5) 0)) list-1)
    '(5 5 5 5)
     "filter by function failed")

(ASSERT_EQUAL
    (filter (lambda (x) (> x 50)) list-1)
    '(368 54 76 86)
     "filter by function '>' failed")

; ---- Any ----
(define (greater10 x) (> x 10))
(ASSERT_TRUE
    (any greater10 '(1 2 5 8 7 4 12 5 0 3 2))
    "any: 12 is greater then 10")
(ASSERT_FALSE
    (any greater10 '(1 2 5 8 7 4 6 5 0 3 2))
    "any: nothing is greater then 10")

; ---- All ----
(define (less5 x) (< x 5))
(ASSERT_TRUE
    (all less5 '( 2 4 0 3 2))
    "all: each number is less then 5")
(ASSERT_FALSE
    (all less5 '( 2 4 0 5 2))
    "all: 5 is not less then 5")

; ---- Map ----
(ASSERT_EQUAL
    (map (lambda (x) (if (< x 10) '- '+)) '( 3 14 10 9 12))
    '(- + + - +)
    "map: test failed")


; ---- VARIABLE ARGUMENT LENGTH ---
(ASSERT_EQUAL
    ((lambda varargs (length varargs)) 1 2 3 4 5 6 7 8)
    8
    "lambda should have variable arg list of length 8")


(define sum (lambda l (apply + l)))
(ASSERT_EQUAL
    (sum 5 6 8 7 4 5 5 9 91 9 5 9 54 8945 54 45 4 5 848)
    (+   5 6 8 7 4 5 5 9 91 9 5 9 54 8945 54 45 4 5 848)
    "varargs sum failed")

; ---- CAR, CDR etc. ---
(ASSERT_EQUAL
    (caar '( (1 2) (3 4) 5 ))
    1
    "caar failed")
(ASSERT_EQUAL
    (cadr '( (1 2) (3 4) 5 ))
    '(3 4)
    "cadr failed")
(ASSERT_EQUAL
    (cdar '( (1 2) (3 4) 5 ))
    '(2)
    "cdar failed")
(ASSERT_EQUAL
    (cddr '( (1 2) (3 4) 5 ))
    '(5)
    "cddr failed")
(ASSERT_EQUAL
    (caddr '( (1 2) (3 4) 5 ))
    5
    "caddr failed")
(ASSERT_EQUAL
    (cadddr '( 1 2 3 4 5 6 ))
    4
    "cadddr failed")

(ASSERT_EQUAL
    (first '( (1 2) (3 4) 5 ))
    '(1 2)
    "first failed")
(ASSERT_EQUAL
    (rest '( (1 2) (3 4) 5 ))
    '((3 4) 5)
    "first failed")
(ASSERT_EQUAL
    (last '( (1 2) (3 4) 5 ))
    5
    "last failed")

;
; ---- TEST SORT ----
;
 (display "  test sort")(newline)

(ASSERT_EQUAL (select-sort list-1) list-1-sorted "select-sort failed")
(ASSERT_EQUAL (btree-sort list-1) list-1-sorted "btree-sort failed")
(ASSERT_EQUAL (quick-sort list-1) list-1-sorted "quick-sort failed")

;
; ---- TEST MATH ----
;
 (display "  test math")(newline)

; ---- SQR ----
(ASSERT_EQUAL
    (sqr 2)
    4
    "2^2 should be 4")
(ASSERT_EQUAL
    (sqr 1)
    1
    "1^2 should be 1")
(ASSERT_EQUAL
    (sqr 0)
    0
    "0^2 should be 0")
(ASSERT_EQUAL
    (sqr 12)
    144
    "12^2 should be 144")


; ---- SQRT ----
(ASSERT_EQUAL
    (sqrt 0)
    0
    "(sqrt 0) failed")
(ASSERT_EQUAL
    (sqrt 1)
    1
    "(sqrt 1) failed")
(ASSERT_EQUAL
    (sqrt 2)
    1
    "(sqrt 2) failed")
(ASSERT_EQUAL
    (sqrt 4)
    2
    "(sqrt 4) failed")
(ASSERT_EQUAL
    (sqrt 64)
    8
    "(sqrt 8) failed")
(ASSERT_EQUAL
    (sqrt (sqr 123))
    123
    "(sqrt (sqr 123)) failed")
(ASSERT_EQUAL
    (sqrt (+ (sqr 3) (sqr 4)))
    5
    "Pythagoras test failed")


;
; ---- TEST RANDOM ----
;
 (display "  test random")(newline)

; ---- random ----

(ASSERT_EQUAL (random 1) 0 "random of modulus 1 should be 0")
(ASSERT_TRUE (< (random 2) 2) "random of modulus 2 should be less then 2")

; ---- random-list ----

(define l (random-list 10 20))
(ASSERT_EQUAL
    (length l)
    20
    "random list should be of length 20")
(ASSERT_TRUE
    (all (lambda (x) (< x 10)) l)
    "random list should have all values less then 20")
(ASSERT_TRUE
    (all (lambda (x) (>= x 0)) l)
    "random list should have all values greater or equals then 0")


;
; ---- TEST LOGICAL OPERATORS ----
;
 (display "  test logical operators")(newline)

(ASSERT_TRUE
    (and #t #t #t #t #t)
    "and: multiple #t should result in #t")

(ASSERT_FALSE
    (and #t #t #t #f #t)
    "and: one #f should result in #f")

(ASSERT_FALSE
    (and #f #t #t #t #t)
    "and: one #f should result in #f (2)")

(ASSERT_TRUE
    (or #f #f #t #f #t)
    "or: some #t should result in #t")

(ASSERT_FALSE
    (or #f #f #f #f #f)
    "or: all #f should result in #f")

;
; --- CHECK ASSERTIONS ---
;
; Should be last statement
(CHECK_ASSERTS)