#include "scheme.h"
#include "time.h"

ScmObjPtr SCM_TRUE_OBJ;
ScmObjPtr SCM_FALSE_OBJ;
ScmObjPtr SCM_NIL_OBJ;
ScmObjPtr SCM_VOID_OBJ;
ScmObjPtr SCM_EOF_OBJ;

ScmEnvPtr SCM_GLOBAL_ENV;

bool      RESUME_ON_ERROR = false;

bool      SCM_EXIT = false;
bool      SCM_EXIT_CODE = 0;

void AFTER_ERROR() {
    if (RESUME_ON_ERROR) {
        longjmp(_jmpBuffer, 1);
    } else {
        exit(100);
    }
}

void initEnvNatives(ScmEnvPtr env) {

    /* Seed the random number generator */
    srand((unsigned int)time(NULL));

    /*
     * Add native functions
     */

    /* Math ops */
    defineEnvValue(env, TMP_PUSH(makeSymbol("+")), TMP_PUSH(makeNativeFunc(nativePlus))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("*")), TMP_PUSH(makeNativeFunc(nativeProduct))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("-")), TMP_PUSH(makeNativeFunc(nativeMinus))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("/")), TMP_PUSH(makeNativeFunc(nativeDiv))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("div")), TMP_PUSH(makeNativeFunc(nativeDiv))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("mod")), TMP_PUSH(makeNativeFunc(nativeMod))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("%")), TMP_PUSH(makeNativeFunc(nativeMod))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("sqrt")), TMP_PUSH(makeNativeFunc(nativeSqrt))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("random")), TMP_PUSH(makeNativeFunc(nativeRandom))); TMP_POP2();

    /* Comparison */
    defineEnvValue(env, TMP_PUSH(makeSymbol(">")), TMP_PUSH(makeNativeFunc(nativeGreaterThen))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("=")), TMP_PUSH(makeNativeFunc(nativeEqualsInt))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("equal?")), TMP_PUSH(makeNativeFunc(nativeEqual))); TMP_POP2();

    /* Predicates */
    defineEnvValue(env, TMP_PUSH(makeSymbol("boolean?")), TMP_PUSH(makeNativeFunc(nativeIsBoolean))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("env?")), TMP_PUSH(makeNativeFunc(nativeIsEnv))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("nil?")), TMP_PUSH(makeNativeFunc(nativeIsNil))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("number?")), TMP_PUSH(makeNativeFunc(nativeIsNumber))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("procedure?")), TMP_PUSH(makeNativeFunc(nativeIsProcedure))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("string?")), TMP_PUSH(makeNativeFunc(nativeIsString))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("symbol?")), TMP_PUSH(makeNativeFunc(nativeIsSymbol))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("void?")), TMP_PUSH(makeNativeFunc(nativeIsVoid))); TMP_POP2();

    /* List operators */
    defineEnvValue(env, TMP_PUSH(makeSymbol("cons")), TMP_PUSH(makeNativeFunc(nativeCons))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("list")), TMP_PUSH(makeNativeFunc(nativeList))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("car")), TMP_PUSH(makeNativeFunc(nativeCar))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("cdr")), TMP_PUSH(makeNativeFunc(nativeCdr))); TMP_POP2();

    /* IO operators */
    defineEnvValue(env, TMP_PUSH(makeSymbol("display")), TMP_PUSH(makeNativeFunc(nativeDisplay))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("newline")), TMP_PUSH(makeNativeFunc(nativeNewline))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("load")), TMP_PUSH(makeNativeFunc(nativeLoad))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("read-number")), TMP_PUSH(makeNativeFunc(nativeReadNumber))); TMP_POP2();

    /* System operators */
    defineEnvValue(env, TMP_PUSH(makeSymbol("time")), TMP_PUSH(makeNativeSyntax(nativeTime))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("exit")), TMP_PUSH(makeNativeFunc(nativeExit))); TMP_POP2();

    /*
     * Add native syntax
     */

    /* Environment manipulation */
    defineEnvValue(env, TMP_PUSH(makeSymbol("define")), TMP_PUSH(makeNativeSyntax(nativeDefine))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("set!")), TMP_PUSH(makeNativeSyntax(nativeSetEx))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("let")), TMP_PUSH(makeNativeSyntax(nativeLet))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("lambda")), TMP_PUSH(makeNativeSyntax(nativeLambda))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("env")), TMP_PUSH(makeNativeSyntax(nativePrintEnv))); TMP_POP2();

    /* Heap */
    defineEnvValue(env, TMP_PUSH(makeSymbol("heap")), TMP_PUSH(makeNativeSyntax(nativePrintHeap))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("gc")), TMP_PUSH(makeNativeFunc(nativeGc))); TMP_POP2();

    /* Stack */
    defineEnvValue(env, TMP_PUSH(makeSymbol("stack")), TMP_PUSH(makeNativeSyntax(nativePrintStack))); TMP_POP2();

    /* Evaluation */
    defineEnvValue(env, TMP_PUSH(makeSymbol("quote")), TMP_PUSH(makeNativeSyntax(nativeQuote))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("eval")), TMP_PUSH(makeNativeSyntax(nativeEval))); TMP_POP2();

    /* Condition*/
    defineEnvValue(env, TMP_PUSH(makeSymbol("if")), TMP_PUSH(makeNativeSyntax(nativeIf))); TMP_POP2();
    defineEnvValue(env, TMP_PUSH(makeSymbol("cond")), TMP_PUSH(makeNativeSyntax(nativeCond))); TMP_POP2();
}


void initSingletonObjects() {
    SCM_TRUE_OBJ = makeSingletonSymbol(TAG_TRUE);
    SCM_FALSE_OBJ = makeSingletonSymbol(TAG_FALSE);
    SCM_NIL_OBJ = makeSingletonSymbol(TAG_NIL);
    SCM_VOID_OBJ = makeSingletonSymbol(TAG_VOID);
    SCM_EOF_OBJ = makeSingletonSymbol(TAG_EOF);
}



static void initSchemeLibraries(ScmEnvPtr env, char** libraries, unsigned librariesCount) {
    for (int i = 0; i < librariesCount; ++i) {
        evalFile(makeString(libraries[i]), env, REPL_DEFAULT_CONFIG);
    }
}

static void initSchemeCoreLibrary(ScmEnvPtr env) {
    evalFile(makeString("init.scm"), env, REPL_DEFAULT_CONFIG);
}

void runBenchmark(ScmConfigPtr configPtr) {
    unsigned chunkSize = configPtr->heapChunkSize,
             maxChunksCount = configPtr->heapMaxChunksCount,
             minStackSize = configPtr->stackInitSize,
             maxStackSize = configPtr->stackMaxSize;

    initStack(minStackSize, maxStackSize);
    initHeap(chunkSize, maxChunksCount);
    initSingletonObjects();
    SCM_GLOBAL_ENV = makeEnv(NULL);
    initEnvNatives(SCM_GLOBAL_ENV);
    initSchemeCoreLibrary(SCM_GLOBAL_ENV);

    evalFile(makeString("benchmark.scm"), SCM_GLOBAL_ENV, REPL_CONFIG_PRINT_RESULT);

    destroyHeap();
    destroyStack();
}

void runScmTests() {
    unsigned chunkSize = 4 * 1024 * 1024,
             maxChunksCount = 16,
             minStackSize = 100,
             maxStackSize = 100;

    initStack(minStackSize, maxStackSize);
    initHeap(chunkSize, maxChunksCount);
    initSingletonObjects();
    ScmEnvPtr env = makeEnv(NULL);
    initEnvNatives(env);
    initSchemeCoreLibrary(env);


    runTests();


    destroyHeap();
    /* Refator tests to not remove main stack */
}


void destroyScm() {

    destroyHeap();
    destroyStack();

}

void initScm(ScmConfigPtr scmConfig) {

    /* Print config */
    if (scmConfig->verbose) {
        printConfig(scmConfig);
    }

    /* 1) Tests */
    if (scmConfig->runTest) {
        runScmTests();
    }

    if (scmConfig->runBenchmark) {
        runBenchmark(scmConfig);
    }

    /* 2) init scheme environment */

    RESUME_ON_ERROR = scmConfig->resumeOnError;
    initStack(scmConfig->stackInitSize, scmConfig->stackMaxSize);
    initHeap(scmConfig->heapChunkSize, scmConfig->heapMaxChunksCount);
    initSingletonObjects();
    SCM_GLOBAL_ENV = makeEnv(NULL);
    initEnvNatives(SCM_GLOBAL_ENV);
    initSchemeLibraries(SCM_GLOBAL_ENV, scmConfig->libraries, scmConfig->librariesCnt);

}