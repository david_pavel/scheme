(display "Hello, guess a number!") (newline)

(define >msg '("It is too large" "Try smaller" "Little bit lower"))
(define <msg '("It is too small" "Try larger" "My number is greater then yours"))

(define (rand-msg l)
    (list-ref l (random (length l))))

(define (guess-game number)
    (define x (read-number))
    (cond ((= x number)
                (display "You win. The number was " number) (newline))
          ((> x number)
                (display (rand-msg >msg)) (newline)
                (guess-game number))
          ((< x number)
                (display (rand-msg <msg)) (newline)
                (guess-game number))
    )
)

(guess-game (random 100))