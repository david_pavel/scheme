;(maxTF – klidTF) x intenzita % + klidTF = tréninkové pásmo
(define (maxTF klidTF vek intenzita)
    (/
        (+
            (* (- 220 vek klidTF) intenzita)
            (* klidTF 100))
        100
    )
)

((lambda ()
    (display "=== Výpočet tepové frekvence pro běh ===") (newline)
    (display "Klidový tep: ")
    (define klid (read-number))
    (display "Intenzita (v %): ")
    (define intenzita (read-number))
    (display "Věk: ")
    (define vek (read-number))

    (display "Tepové rozmezí " (maxTF klid vek intenzita) " - " (maxTF klid vek (+ intenzita 10)))(newline)
))
