(define (counter start)
	(define (increment value)
		(set! start (+ start value))
		start
	)
	increment
)

(define c5 (counter 5))
(define c3 (counter 3))

