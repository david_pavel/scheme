#include <string.h>
#include "scheme.h"

#define DEBUG_OBJECTS false

ScmObjPtr makeInteger(int number) {
    DEBUG(DEBUG_OBJECTS, printf("Allocate memory for INT %d\n", number));
    ScmIntPtr integerPtr = (ScmIntPtr)scmMalloc(sizeof(ScmInt), TAG_INT);
    integerPtr->value = number;
    return (ScmObjPtr)integerPtr;
}

ScmObjPtr makeString(char* str) {
    DEBUG(DEBUG_OBJECTS, printf("Allocate memory for STRING %s\n", str));
    size_t len = strlen(str);

    /* -1 for char[1] ins ScmStr */
    /* +1 for '\0' character */
    ScmStrPtr stringPtr = (ScmStrPtr)scmMalloc(sizeof(ScmStr) - 1 + len + 1, TAG_STRING);
    strcpy(stringPtr->value, str);
    return (ScmObjPtr)stringPtr;
}

ScmObjPtr makeSymbol(char* sym) {
    DEBUG(DEBUG_OBJECTS, printf("Allocate memory for SYMBOL %s\n", sym));
    size_t len = strlen(sym);

    /* -1 for char[1] ins ScmSym */
    /* +1 for '\0' character */
    ScmSymPtr symbolPtr = (ScmSymPtr)scmMalloc(sizeof(ScmSym) - 1 + len + 1, TAG_SYMBOL);
    strcpy(symbolPtr->value, sym);
    return (ScmObjPtr)symbolPtr;
}


ScmObjPtr makeSingletonSymbol(TAG tag) {
    DEBUG(DEBUG_OBJECTS, printf("Allocate memory for %s\n", TAG_NAME(tag)));
    ScmObjHdrPtr objPtr = (ScmObjHdrPtr)scmMalloc(sizeof(ScmObjHdr), tag);
    return (ScmObjPtr)objPtr;
}

ScmObjPtr makeCons(ScmObjPtr car, ScmObjPtr cdr) {
    DEBUG(DEBUG_OBJECTS, printf("Allocate memory for CONS\n"));
    TMP_PUSH2(car, cdr);
    ScmConsPtr consPtr = (ScmConsPtr)scmMalloc(sizeof(ScmCons), TAG_CONS);
    TMP_POP2();
    consPtr->car = car;
    consPtr->cdr = cdr;
    return (ScmObjPtr)consPtr;
}

ScmObjPtr makeNativeFunc(FUNC_PTR func) {
    DEBUG(DEBUG_OBJECTS, printf("Allocate memory for NATIVE FUNCTION\n"));
    ScmNativeFuncPtr funcObjPtr = (ScmNativeFuncPtr)scmMalloc(sizeof(ScmNativeFunc), TAG_NATIVE_FUNC);
    funcObjPtr->func = func;
    return (ScmObjPtr)funcObjPtr;
}


ScmObjPtr makeNativeSyntax(SYNTAX_PTR func) {
    DEBUG(DEBUG_OBJECTS, printf("Allocate memory for NATIVE SYNTAX\n"));
    ScmNativeSyntaxPtr funcObjPtr = (ScmNativeSyntaxPtr)scmMalloc(sizeof(ScmNativeSyntax), TAG_NATIVE_SYNTAX);
    funcObjPtr->func = func;
    return (ScmObjPtr)funcObjPtr;
}

ScmObjPtr makeUserFunc(ScmObjPtr argList, ScmObjPtr bodyList, ScmEnvPtr parentEnv) {
    DEBUG(DEBUG_OBJECTS, printf("Allocate memory for USER FUNCTION\n"));
    TMP_PUSH3(argList, bodyList, parentEnv);
    ScmUserFuncPtr funcObjPtr = (ScmUserFuncPtr)scmMalloc(sizeof(ScmUserFunc), TAG_USER_FUNC);
    TMP_POP3();
    funcObjPtr->argList = argList;
    funcObjPtr->bodyList = bodyList;
    funcObjPtr->parentEnv = parentEnv;
    return (ScmObjPtr)funcObjPtr;
}