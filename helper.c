#include "scheme.h"

char* TAG_NAME(TAG tag)
{
    switch (tag)
    {
        case TAG_INT: return "INT";
        case TAG_STRING: return "STRING";
        case TAG_SYMBOL: return "SYMBOL";
        case TAG_TRUE: return "TRUE";
        case TAG_FALSE: return "FALSE";
        case TAG_NIL: return "NIL";
        case TAG_VOID: return "VOID";
        case TAG_EOF: return "EOF";
        case TAG_CONS: return "CONS";
        case TAG_NATIVE_FUNC: return "NATIVE_FUNC";
        case TAG_NATIVE_SYNTAX: return "NATIVE_SYNTAX";
        case TAG_USER_FUNC: return "USER_FUNC";
        case TAG_ENV: return "ENV";
        case TAG_ENV_ENTRY_ARRAY: return "ENV_ENTRY_ARRAY";
        case TAG_FREE: return "FREE";
        default: return "UNKNOWN TAG";
    }
}

void printHumanReadableSize(unsigned long size, FILE* out)
{
    if (size == 0) {
        fprintf(out, "0B");
    }

    char gbStr[10],  mbStr[10],  kbStr[10],  bStr[10];

    unsigned long gb, mb, kb, b;
    kb = size / 1024;
    b = size % 1024;

    mb = kb / 1024;
    kb = kb % 1024;

    gb = mb / 1024;
    mb = mb % 1024;

    if (gb > 0) {
        sprintf(gbStr, "%ldGB ", gb);
    } else {
        gbStr[0]='\0';
    }

    if (mb > 0) {
        sprintf(mbStr, "%ldMB ", mb);
    } else {
        mbStr[0]='\0';
    }

    if (kb > 0) {
        sprintf(kbStr, "%ldkB ", kb);
    } else {
        kbStr[0]='\0';
    }

    if (b > 0) {
        sprintf(bStr, "%ldB ", b);
    } else {
        bStr[0]='\0';
    }

    fprintf(out, "%s%s%s%s", gbStr, mbStr, kbStr, bStr);
}