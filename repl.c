#include "scheme.h"

void repl(FILE *in, FILE *out, ScmEnvPtr env, REPL_CONFIG config) {
    for (;;) {
        if (SCM_EXIT) {
            break;
        }

        if (CONFIG_HAS(config, REPL_CONFIG_SETJMP)) {
            setjmp(_jmpBuffer);
        }
        if (CONFIG_HAS(config, REPL_CONFIG_SHOW_PROMPT)) {
            fprintf(out, "> ");
            fflush(out);
        }

        /* READ */
        ScmObjPtr objPtr = read(in);
        if (IS_EOF(objPtr)) {
            return;
        }

        /* EVAL */
        TMP_PUSH(objPtr);
        objPtr = eval(objPtr, env);
        TMP_POP();

        /* PRINT */
        if (CONFIG_HAS(config, REPL_CONFIG_PRINT_RESULT)) {
            print(objPtr, out);
        }
        if (CONFIG_HAS(config, REPL_CONFIG_PRINT_NEWLINE_AFTER_RESULT)) {
            fprintf(out, "\n");
        }
    }
}

void file(char *fileName, ScmEnvPtr env, REPL_CONFIG config) {
    if (setjmp(_jmpBuffer)) {
        return; /* Eval file failed */
    }
    evalFile(makeString(fileName), env, config);
}