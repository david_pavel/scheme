#include "scheme.h"

#define DEBUG_GC false
#define PRINT_GC_RUN false

#define SET_MARKED(objPtr) (((ScmObjPtr)objPtr)->header.marked = true)
#define SET_UNMARKED(objPtr) (((ScmObjPtr)objPtr)->header.marked = false)
#define IS_MARKED(objPtr) (((ScmObjPtr)objPtr)->header.marked == true)

static void mark(ScmObjPtr objPtr) {
    if (objPtr == NULL) {
        DEBUG(DEBUG_GC, printf("CAN NOT MARK NULL OBJECT!\n"));
        return;
    }
    if (IS_MARKED(objPtr)) {
        return; /* Already marked */
    }

    SET_MARKED(objPtr);

    switch (TAG_OF(objPtr)) {
        case TAG_INT:
        case TAG_STRING:
        case TAG_SYMBOL:
        case TAG_FALSE:
        case TAG_TRUE:
        case TAG_NIL:
        case TAG_VOID:
        case TAG_EOF:
        case TAG_FREE:
        case TAG_NATIVE_FUNC:
        case TAG_NATIVE_SYNTAX:
            /* nothing to mark */
            break;
        case TAG_CONS:
            mark(CAR_OF(objPtr));
            mark(CDR_OF(objPtr));
            break;
        case TAG_USER_FUNC:
            mark(objPtr->userFunc.argList);
            mark(objPtr->userFunc.bodyList);
            mark((ScmObjPtr)objPtr->userFunc.parentEnv);
            break;
        case TAG_ENV:
            if ((ScmObjPtr)objPtr->env.parent != NULL) {
                mark((ScmObjPtr)objPtr->env.parent);
            }
            mark((ScmObjPtr)objPtr->env.entryArray);
            break;
        case TAG_ENV_ENTRY_ARRAY:
            for (int i = 0; i < objPtr->envEntryArray.count; ++i) {
                mark(objPtr->envEntryArray.entries[i].key);
                mark(objPtr->envEntryArray.entries[i].value);
            }
            break;
        default:
            printHeap(stderr);
            print(objPtr, stderr);
            errorf("GC: Mark for TAG '%s' (%d) [%p] not implemented yet\n", TAG_NAME(TAG_OF(objPtr)), TAG_OF(objPtr), (void*)objPtr);
    }
}

static void markStack() {
    unsigned stackSize;
    ScmObjPtr* stack = getStack(&stackSize);

    DEBUG(DEBUG_GC, printStack(stdout));

    for (int i = 0; i < stackSize; ++i) {
        mark(stack[i]);
    }
}

static void markGlobals() {
    mark(SCM_TRUE_OBJ);
    mark(SCM_FALSE_OBJ);
    mark(SCM_NIL_OBJ);
    mark(SCM_VOID_OBJ);
    mark(SCM_EOF_OBJ);

    mark((ScmObjPtr)SCM_GLOBAL_ENV);
}

void sweep() {
    POINTER lastFreeCell = NULL;
    unsigned lastChunkIndex = -1;
    HeapIterator* iterator = heapIteratorCreate();

    DEBUG(DEBUG_GC, printHeap(stdout));

    /* Loop all the heap cells */
    do {
        if (iterator->currentChunkIndex != lastChunkIndex) {
            /* Moved to another chunk. Clear the last free cell pointer */
            lastFreeCell = NULL;
            lastChunkIndex = iterator->currentChunkIndex;
        }

        if (IS_MARKED(iterator->currentCell)) {
            /* Live object */
            SET_UNMARKED(iterator->currentCell);
            lastFreeCell = NULL;
            continue;
        } else {
            /* decrease allocated memory in the current chunk */
            if (TAG_OF(iterator->currentCell) != TAG_FREE) {
                DEBUG(DEBUG_GC, printf("!!! {-} %u freed memory cell of tag %s\n", ((ScmObjPtr) iterator->currentCell)->header.size, TAG_NAME(TAG_OF(iterator->currentCell))));
                (*iterator->currentChunkMemory) = (*iterator->currentChunkMemory) - ((ScmObjPtr) iterator->currentCell)->header.size;
            }

            /* Object to discard */
            if (lastFreeCell != NULL) {
                /* Enlarge last free cell */
                ((ScmObjPtr)lastFreeCell)->header.size += ((ScmObjPtr)iterator->currentCell)->header.size;
            } else {
                /* Make current cell free */
                ((ScmObjPtr)iterator->currentCell)->header.tag = TAG_FREE;
                lastFreeCell = iterator->currentCell;
            }
        }
    } while (heapIteratorNext(iterator));

    heapIteratorDestroy(iterator);
    
    // Indicate gc sweeped the heap (current free cell pointer should be reset)
    resetHeapCurrentFreeCell();
}

/* Run garbage collector */
void scmGC() {

    DEBUG(PRINT_GC_RUN, printf("Running GC: "));
    DEBUG(PRINT_GC_RUN, printHumanReadableSize(heapTotalAllocatedMemory(), stdout));

    /* MARK */
    markGlobals();
    markStack();

    /* SWEEP */
    sweep();

    DEBUG(PRINT_GC_RUN, printf(" -> "));
    DEBUG(PRINT_GC_RUN, printHumanReadableSize(heapTotalAllocatedMemory(), stdout));
    DEBUG(PRINT_GC_RUN, printf("\n"));
}