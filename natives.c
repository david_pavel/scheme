#include "scheme.h"
#include "string.h"
#include <time.h>


#define DEBUG_NATIVES false
/* ======================== */
/* === Native FUNCTIONS === */
/* ======================== */

ScmObjPtr nativePlus(int evaluatedArgsCount) {
    ScmObjPtr arg;
    int result = 0;

    for (int i = 0; i < evaluatedArgsCount; ++i) {
        arg = POP();
        if (!IS_INT(arg)) {
            errorfa("Native 'plus' function argument %d is not a number but: ", arg,  evaluatedArgsCount - i);
        }
        // add number
        result += INT_VALUE(arg);
    }
    return makeInteger(result);
}

ScmObjPtr nativeProduct(int evaluatedArgsCount) {
    ScmObjPtr arg;
    int result = 1;

    for (int i = 0; i < evaluatedArgsCount; ++i) {
        arg = POP();
        if (!IS_INT(arg)) {
            errorfa("Native 'product' function argument %d is not a number but: ", arg,  evaluatedArgsCount - i);
        }
        // add number
        result *= INT_VALUE(arg);
    }
    return makeInteger(result);
}

ScmObjPtr nativeMinus(int evaluatedArgsCount) {
    ScmObjPtr firstArg, otherArg;
    int result;

    if (evaluatedArgsCount < 1) {
        error("Native 'minus' function needs at least one argument");
    }

    /* first argument */
    firstArg = SP_GET_AT(-evaluatedArgsCount);
    if (!IS_INT(firstArg)) {
        errorfa("Native 'minus' function argument %d is not a number but: ", firstArg,  evaluatedArgsCount);
    }
    if (evaluatedArgsCount == 1) {
       result = -INT_VALUE(firstArg);
    } else {
        result = INT_VALUE(firstArg);
    }

    /* rest arguments */
    for (int i = 1; i < evaluatedArgsCount; ++i) {
        otherArg = SP_GET_AT(-evaluatedArgsCount + i);
        if (!IS_INT(otherArg)) {
            errorfa("Native 'minus' function argument %d is not a number but: ", otherArg,  evaluatedArgsCount - i);
        }
        // subtract number
        result -= INT_VALUE(otherArg);
    }

    /* remove args from stack */
    SP_DEC(evaluatedArgsCount);

    return makeInteger(result);
}

ScmObjPtr nativeDiv(int evaluatedArgsCount) {
    ScmObjPtr firstArg, otherArg;
    double result;

    if (evaluatedArgsCount < 2) {
        error("Native 'div' function needs at least two arguments");
    }

    /* first argument */
    firstArg = SP_GET_AT(-evaluatedArgsCount);
    if (!IS_INT(firstArg)) {
        errorfa("Native 'div' function argument %d is not a number but: ", firstArg,  evaluatedArgsCount);
    }
    if (evaluatedArgsCount == 1) {
       result = -INT_VALUE(firstArg);
    } else {
        result = INT_VALUE(firstArg);
    }

    /* rest arguments */
    for (int i = 1; i < evaluatedArgsCount; ++i) {
        otherArg = SP_GET_AT(-evaluatedArgsCount + i);
        if (!IS_INT(otherArg)) {
            errorfa("Native 'div' function argument %d is not a number but: ", otherArg,  evaluatedArgsCount - i);
        }
        if (INT_VALUE(otherArg) == 0) {
            error("Division by zero");
        }

        // subtract number
        result /= INT_VALUE(otherArg);
    }

    /* remove args from stack */
    SP_DEC(evaluatedArgsCount);

    return makeInteger((int)result);
}

ScmObjPtr nativeMod(int evaluatedArgsCount) {
    ScmObjPtr firstArg, secondArg;

    if (evaluatedArgsCount != 2) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'mod' expects exactly two arguments");
    }

    secondArg = POP();
    firstArg = POP();

    if (!IS_INT(firstArg)) {
        errora("Native 'mod' expects number as first arg, but found: ", firstArg);
    }
    if (!IS_INT(secondArg)) {
        errora("Native 'mod' expects number as second arg, but found: ", secondArg);
    }

    return makeInteger(INT_VALUE(firstArg) % INT_VALUE(secondArg));
}

/* http://www.codecodex.com/wiki/Calculate_an_integer_square_root#C */
unsigned int _isqrt(unsigned int x)
{
    register unsigned int op, res, one;

    op = x;
    res = 0;

    /* "one" starts at the highest power of four <= than the argument. */
    one = 1 << 30;  /* second-to-top bit set */
    while (one > op) one >>= 2;

    while (one != 0) {
        if (op >= res + one) {
            op -= res + one;
            res += one << 1;  // <-- faster than 2 * one
        }
        res >>= 1;
        one >>= 2;
    }
    return res;
}

ScmObjPtr nativeSqrt(int evaluatedArgsCount) {
    ScmObjPtr firstArg;

    if (evaluatedArgsCount != 1) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'sqrt' expects exactly one argument");
    }

    firstArg = POP();

    if (!IS_INT(firstArg)) {
        errora("Native 'sqrt' expects number as argument, but found: ", firstArg);
    }

    return makeInteger(_isqrt((unsigned int)INT_VALUE(firstArg)));
}

ScmObjPtr nativeReadNumber(int evaluatedArgsCount) {
    if (evaluatedArgsCount > 0) {
        error("Native 'read-number' function needs no argument");
    }

    int number;
    scanf("%d", &number);

    return makeInteger(number);

}
ScmObjPtr nativeLoad(int evaluatedArgsCount) {
    ScmObjPtr arg;

    if (evaluatedArgsCount < 1) {
        error("Native 'load' function needs at least one argument");
    }

    for (int i = 0; i < evaluatedArgsCount; ++i) {
        arg = POP();
        if (!IS_STRING(arg)) {
            errorfa("Native 'load' function argument %d is not a string but: ", arg,  evaluatedArgsCount - i);
        }
        // eval file
        evalFile(arg, SCM_GLOBAL_ENV, REPL_DEFAULT_CONFIG);
    }
    return SCM_VOID_OBJ;
}

ScmObjPtr nativeGreaterThen(int evaluatedArgsCount) {
    ScmObjPtr firstArg, secondArg;
    if (evaluatedArgsCount != 2) {
        SP_DEC(evaluatedArgsCount);
        errorf("Native 'greater then' needs 2 arguments, but provided %d", evaluatedArgsCount);
    }

    /* Extract arguments */
    secondArg = POP();
    firstArg = POP();

    /* Process comparison */
    switch (TAG_OF(firstArg)) {
        case TAG_INT:
            if (!IS_INT(secondArg)) {
                errorf("Native 'greater then' can compare integer types only. Provided %d TAG instead as second argument.",TAG_OF(firstArg));
            }
            return INT_VALUE(firstArg) > INT_VALUE(secondArg) ? SCM_TRUE_OBJ : SCM_FALSE_OBJ;
        default:
            errorf("Native 'greater then' can compare integer types only. Provided %d TAG instead as first argument.",TAG_OF(firstArg));
            return NULL; /* make compiler happy */
    }
}

ScmObjPtr nativeEqualsInt(int evaluatedArgsCount) {
    ScmObjPtr firstObj, secondObj;
    if (evaluatedArgsCount != 2) {
        SP_DEC(evaluatedArgsCount);
        error("Native '=' expects 2 arguments: (= <number> <number>)");
    }

    /* args are on the stack in reversed order */
    secondObj = POP();
    firstObj = POP();

    if (!IS_INT(firstObj)) {
        errora("Native '=' compares numbers only, but first argument is ", firstObj);
    }
    if (!IS_INT(secondObj)) {
        errora("Native '=' compares numbers only, but second argument is ", secondObj);
    }
    return INT_VALUE(firstObj) == INT_VALUE(secondObj) ? SCM_TRUE_OBJ : SCM_FALSE_OBJ;
}

ScmObjPtr _nativeEqual(ScmObjPtr firstObj, ScmObjPtr secondObj);
ScmObjPtr _nativeEqualCons(ScmObjPtr firstObj, ScmObjPtr secondObj);

ScmObjPtr _nativeEqualCons(ScmObjPtr firstObj, ScmObjPtr secondObj) {
    if (IS_NIL(firstObj) && IS_NIL(secondObj)) {
        return SCM_TRUE_OBJ;
    }

    if (!IS_CONS(firstObj) || !IS_CONS(secondObj)) {
        return SCM_FALSE_OBJ;
    }

    if (_nativeEqual(CAR_OF(firstObj), CAR_OF(secondObj)) == SCM_TRUE_OBJ) {
        return _nativeEqualCons(CDR_OF(firstObj), CDR_OF(secondObj));
    } else {
        return SCM_FALSE_OBJ;
    }
}

ScmObjPtr _nativeEqual(ScmObjPtr firstObj, ScmObjPtr secondObj) {
    if (TAG_OF(firstObj) != TAG_OF(secondObj)) {
        return SCM_FALSE_OBJ; /* Different type of objects */
    }
    switch (TAG_OF(firstObj)) {
        case TAG_FALSE:
        case TAG_TRUE:
        case TAG_NIL:
        case TAG_VOID:
        case TAG_EOF:
            return SCM_TRUE_OBJ;
        case TAG_INT:
            return TO_SCM_BOOL(INT_VALUE(firstObj) == INT_VALUE(secondObj));
        case TAG_CONS:
            return _nativeEqualCons(firstObj, secondObj);
        case TAG_STRING:
        case TAG_SYMBOL:
            return TO_SCM_BOOL(strcmp(STR_VALUE(firstObj), STR_VALUE(secondObj)) == 0);
        case TAG_NATIVE_FUNC:
            return TO_SCM_BOOL(firstObj->nativeFunc.func == secondObj->nativeFunc.func);
        case TAG_NATIVE_SYNTAX:
            return TO_SCM_BOOL(firstObj->nativeSyntax.func == secondObj->nativeSyntax.func);
        default:
            errorf("Native 'equal?': unknown tag %d to compare.", TAG_OF(firstObj));
            return NULL; /* make compiler happy */
    }
}

ScmObjPtr nativeEqual(int evaluatedArgsCount) {
    ScmObjPtr firstObj, secondObj;
    if (evaluatedArgsCount != 2) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'equal?' expects 2 arguments: (equal? <expression-1> <expression-2>)");
    }

    /* args are on the stack in reversed order */
    secondObj = POP();
    firstObj = POP();

    return _nativeEqual(firstObj, secondObj);
}

ScmObjPtr nativeIsNil(int evaluatedArgsCount) {
    if (evaluatedArgsCount != 1) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'nil?' expects 1 argument: (nil? <object>)");
    }
    return TO_SCM_BOOL(IS_NIL(POP()));
}

ScmObjPtr nativeIsVoid(int evaluatedArgsCount) {
    if (evaluatedArgsCount != 1) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'void?' expects 1 argument: (void? <object>)");
    }
    return TO_SCM_BOOL(IS_VOID(POP()));
}

ScmObjPtr nativeIsString(int evaluatedArgsCount) {
    if (evaluatedArgsCount != 1) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'string?' expects 1 argument: (string? <object>)");
    }
    return TO_SCM_BOOL(IS_STRING(POP()));
}

ScmObjPtr nativeIsSymbol(int evaluatedArgsCount) {
    if (evaluatedArgsCount != 1) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'symbol?' expects 1 argument: (symbol? <object>)");
    }
    return TO_SCM_BOOL(IS_SYMBOL(POP()));
}

ScmObjPtr nativeIsNumber(int evaluatedArgsCount) {
    if (evaluatedArgsCount != 1) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'number?' expects 1 argument: (number? <object>)");
    }
    return TO_SCM_BOOL(IS_INT(POP()));
}

ScmObjPtr nativeIsProcedure(int evaluatedArgsCount) {
    if (evaluatedArgsCount != 1) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'procedure?' expects 1 argument: (procedure? <object>)");
    }
    ScmObjPtr objPtr = POP();
    return TO_SCM_BOOL(IS_USER_FUNC(objPtr) || IS_NATIVE_FUNC(objPtr) || IS_NATIVE_SYNTAX(objPtr));
}

ScmObjPtr nativeIsEnv(int evaluatedArgsCount) {
    if (evaluatedArgsCount != 1) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'env?' expects 1 argument: (env? <object>)");
    }
    return TO_SCM_BOOL(IS_ENVIRONMENT(POP()));
}

ScmObjPtr nativeIsBoolean(int evaluatedArgsCount) {
    if (evaluatedArgsCount != 1) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'boolean?' expects 1 arguments: (= <number> <number>)");
    }
    ScmObjPtr objPtr = POP();
    return TO_SCM_BOOL(IS_TRUE(objPtr) || IS_FALSE(objPtr));
}

ScmObjPtr nativeCar(int evaluatedArgsCount) {
    ScmObjPtr listPtr;
    if (evaluatedArgsCount != 1) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'car' expects 1 arguments (car <list>)");
    }

    listPtr = POP();
    if (!IS_CONS(listPtr)) {
        errora("Native 'car' expects argument of type CONS (car <list>). But provided: ", listPtr);
    }
    return listPtr->cons.car;
}

ScmObjPtr nativeCdr(int evaluatedArgsCount) {
    ScmObjPtr listPtr;
    if (evaluatedArgsCount != 1) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'cdr' expects 1 arguments (cdr <list>)");
    }

    listPtr = POP();
    if (!IS_CONS(listPtr)) {
        errora("Native 'cdr' expects argument of type CONS (cdr <list>). But provided: ", listPtr);
    }
    return listPtr->cons.cdr;
}

ScmObjPtr nativeCons(int evaluatedArgsCount) {
    ScmObjPtr carPtr, cdrPtr;
    if (evaluatedArgsCount != 2) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'cons' expects 2 arguments (cons <object> <cons>)");
    }

    /* args are on the stack in reversed order */
    cdrPtr = POP();
    carPtr = POP();

    if (!IS_CONS(cdrPtr) && !IS_NIL(cdrPtr)) {
        errora("Native 'cons' expects cdr of type CONS or #nil. But provided cdr: ", cdrPtr);
    }
    return makeCons(carPtr, cdrPtr);
}

ScmObjPtr nativeDisplay(int evaluatedArgsCount) {
    ScmObjPtr arg;
    if (evaluatedArgsCount == 0) {
        error("Native 'display' needs at least 1 argument");
    }

    for (int i = evaluatedArgsCount; i > 0; --i) {
        arg = SP_GET_AT(-i);
        if (TAG_OF(arg) == TAG_STRING) {
            /* Print string without quotes */
            fprintf(stdout, "%s", arg->string.value);
        } else {
            print(arg, stdout);
        }
    }
    fflush(stdout);
    SP_DEC(evaluatedArgsCount);
    return SCM_VOID_OBJ;
}

ScmObjPtr nativeNewline(int evaluatedArgsCount) {
    if (evaluatedArgsCount != 0) {
        error("Native 'newline' needs no argument");
    }
    fprintf(stdout, "\n");
    return SCM_VOID_OBJ;
}

ScmObjPtr nativeList(int evaluatedArgsCount) {
    ScmObjPtr argObjPtr, rsltObjPtr = SCM_NIL_OBJ;

    for(int i = 0; i < evaluatedArgsCount; i++) {
        argObjPtr = POP();
        rsltObjPtr = makeCons(argObjPtr, rsltObjPtr);
    }

    return rsltObjPtr;
}

ScmObjPtr nativeExit(int evaluatedArgsCount) {
    ScmObjPtr objPtr;
    int rsltCode = 0;
    if (evaluatedArgsCount > 1) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'exit' needs one arument at most: (exit [result_code])");
    }

    if (evaluatedArgsCount == 1) {
        objPtr = POP();
        if (!IS_INT(objPtr)) {
            errora("Native 'exit' result code must be integer. Provided: ", objPtr);
        }
        rsltCode = INT_VALUE(objPtr);
    }

    SCM_EXIT = true;
    SCM_EXIT_CODE = rsltCode;

    return SCM_VOID_OBJ;
}

static int rand_mod(int mod) {
    int x = rand();
    const int R_MAX = ((RAND_MAX) / mod) * mod;

    while (x > R_MAX) { // discard the result if it is bigger
        x = rand();
    }

    return (x % mod);
}

ScmObjPtr nativeRandom(int evaluatedArgsCount) {
    ScmObjPtr modulusObj;
    if (evaluatedArgsCount != 1) {
        SP_DEC(evaluatedArgsCount);
        error("Native 'random' needs exactly one argument: (random <modulus>)");
    }

    modulusObj = POP();
    if (!IS_INT(modulusObj)) {
        error("Native 'random' needs exactly one argument of INTEGER: (random <modulus>)");
    }

    return makeInteger(rand_mod(INT_VALUE(modulusObj)));
}

/* ===================== */
/* === Native SYNTAX === */
/* ===================== */

ScmObjPtr nativeDefine(int argsCount, ScmEnvPtr env) {
    ScmObjPtr firstArg, secondArg;
    /*
     * Syntax 1:
     * (define x 1)
     * (define y (+ 2 a))
     * (define <symbol> <evaluated-arg>)
     *
     * Syntax 2:
     * (define (fn a b) (+ a b))
     * (define (<symbol> <args-list>...) <body-list>...)
     */
    if (argsCount < 2) {
        SP_DEC(argsCount);
        error("Native 'define' expects at least 2 arguments: (define <symbol> <expression>) or (define (<symbol> <args-list>...) <body-list>...)");
    }

    firstArg = SP_GET_AT(-argsCount);
    secondArg = SP_GET_AT(-argsCount + 1);

    switch (TAG_OF(firstArg)) {
        case TAG_SYMBOL: {
            /* Syntax 1 (define symbol value) */
            if (argsCount != 2) {
                SP_DEC(argsCount);
                error("Native 'define' expects arguments 2 arguments: (define <symbol> <expression>)");
            }
            SP_DEC(argsCount);
            /* Evaluate provided expression */
            TMP_PUSH(firstArg);
            ScmObjPtr evaluatedArg = eval(secondArg, env);
            TMP_POP();
            /* Store evaluated expression in env on key position */
            defineEnvValue(env, firstArg, evaluatedArg);
            return SCM_VOID_OBJ;
        }
        case TAG_CONS: {
            /* Syntax 2 (define (function-name args...) function-bodies...) */
            ScmObjPtr fnName = CAR_OF(firstArg);
            ScmObjPtr argList = CDR_OF(firstArg);
            if (!IS_SYMBOL(fnName)) {
                SP_DEC(argsCount);
                error("Native 'define' expects name of function to be symbol. (define (<symbol> <args-list>...) <body-list>...)");
            }
            /* replace firstArg for fnName -> stack fits the lambda */
            SP_SET_AT(-argsCount, argList);

            // TODO: need to remember fnName
            /* !!! Dirty Hack: pre-save fnName in the environment (to make it persist when GC runs). Assigned for example SCM_EOF_OBJ */
            defineEnvValue(env, fnName, SCM_EOF_OBJ);

            ScmObjPtr lambdaPtr = nativeLambda(argsCount, env);
            defineEnvValue(env, fnName, lambdaPtr);
            return SCM_VOID_OBJ;
        }
        default: {
            SP_DEC(argsCount);
            errora("Native 'define' unsupported type of first argument: ", firstArg);
            return NULL; /* make compiler happy */
        }
    }
}

ScmObjPtr nativeSetEx(int argsCount, ScmEnvPtr env) {
    ScmObjPtr symbol, expression, evaluateExpression;
    /*
     * (set! x 1)
     * (set! y (+ 2 a))
     * (set! <symbol> <evaluated-arg>)
     */
    if (argsCount != 2) {
        SP_DEC(argsCount);
        error("Native 'set!' expects exactly 2 arguments: (set! <existing-symbol> <expression>)");
    }

    expression = POP();
    symbol = POP();

    if (!IS_SYMBOL(symbol)) {
        errora("Native 'set!' expects symbol as first argument, but found: ", symbol);
    }

    TMP_PUSH(symbol);
    evaluateExpression = eval(expression, env);
    TMP_POP();

    if (!setEnvValue(env, symbol, evaluateExpression)) {
        errorfa("Native 'set!': Symbol does not exist. Can not set '%s' value of: ", evaluateExpression, STR_VALUE(symbol));
    }
    return SCM_VOID_OBJ;
}

ScmObjPtr nativeLet(int argsCount, ScmEnvPtr env) {
    ScmObjPtr definitionList, tuple, valueObj, keyObj, evaluatedValueObj,
            resultObj, bodyObj;
    /*
     * (let ((x 1)) x)
     * (let ((y (+ 2 3)) (x 4))  (+ y x))
     * (let ((<symbol-1> <value-1>) ... (<symbol-n> <value-n>)) <bodies>...)
     */
    if (argsCount < 2) {
        SP_DEC(argsCount);
        error("Native 'let' expects at least 2 arguments: (let ((var val) ...) exp1 exp2 ...)");
    }

    /* Read definition list */
    definitionList = SP_GET_AT(-argsCount);
    if (!IS_CONS(definitionList)) {
        SP_DEC(argsCount);
        errora("Native 'let' first argument should be definition list, but found", definitionList);
    }
    /* Create env for definition list */
    ScmEnvPtr letEnv = makeEnv(env);  /* TODO: set env default size same as let assignments count */
TMP_PUSH(env);
    /* Define all symbols from definitionList */
    int definitionCntr = 0;
    while(!IS_NIL(definitionList)) {
        tuple = CAR_OF(definitionList);
        definitionList = CDR_OF(definitionList);
        definitionCntr++;
        /* Validate key */
        keyObj = CAR_OF(tuple);
        if (!IS_SYMBOL(keyObj)) {
            errorfa("Native 'let' definition %d should have 'symbol' as first argument, but found ", CAR_OF(tuple), definitionCntr);
        }
        /* Extract value */
        valueObj = CAR_OF(CDR_OF(tuple));
        evaluatedValueObj = eval(valueObj, env);
        /* Set key-value pair */
        letEnvValue(letEnv, keyObj, evaluatedValueObj);
    }

    /* Read and evaluate body list */
    resultObj = SCM_VOID_OBJ;
    for (int i = argsCount - 1; i >= 1; --i) {
        bodyObj = SP_GET_AT(-i);
        TMP_PUSH(resultObj);
        resultObj = eval(bodyObj, letEnv);
        TMP_POP();
    }
TMP_POP();
    SP_DEC(argsCount);
    return resultObj;
}

ScmObjPtr nativePrintEnv(int evaluatedArgsCount, ScmEnvPtr env) {
    if (evaluatedArgsCount != 0) {
        SP_DEC(evaluatedArgsCount);
        errorf("Native 'print-env' has no arguments but provided %d", evaluatedArgsCount);
    }
    printEnvStdout(env);
    return SCM_VOID_OBJ;
}

ScmObjPtr nativePrintHeap(int evaluatedArgsCount, __attribute__((unused)) ScmEnvPtr env) {
    if (evaluatedArgsCount != 0) {
        SP_DEC(evaluatedArgsCount);
        errorf("Native 'heap' expects no argument but provided %d", evaluatedArgsCount);
    }
    printHeap(stdout);
    return SCM_VOID_OBJ;
}

ScmObjPtr nativePrintStack(int evaluatedArgsCount, __attribute__((unused)) ScmEnvPtr env) {
    if (evaluatedArgsCount != 0) {
        SP_DEC(evaluatedArgsCount);
        errorf("Native 'stack' expects no argument but provided %d", evaluatedArgsCount);
    }
    printStack(stdout);
    return SCM_VOID_OBJ;
}

ScmObjPtr nativeGc(int evaluatedArgsCount) {
    if (evaluatedArgsCount != 0) {
        SP_DEC(evaluatedArgsCount);
        errorf("Native 'gc' expects no argument but provided %d", evaluatedArgsCount);
    }
    scmGC();
    return SCM_VOID_OBJ;
}

ScmObjPtr nativeLambda(int argsCount, __attribute__((unused)) ScmEnvPtr env) {


    ScmObjPtr argList, currentArg, bodyList;
    /*
     * Syntax:
     * (lambda (a) (+ 1 a))
     * (lambda (x y)
     *      (define res (+ (* x x) (* y y)))
     *      (sqrt res))
     * (lambda (<argument-list>) <body-list>)
     * */

    if (argsCount < 2) {
        SP_DEC(argsCount);
        error("Native lamda expects at least 2 arguments: (lambda (<args-list>) <body-list>)");
    }

    /* Load arg list */
    argList = SP_GET_AT(-argsCount);
    DEBUG(DEBUG_NATIVES, printf("NATIVE: lambda with arglist: "));
    DEBUG(DEBUG_NATIVES, print(argList, stdout));
    DEBUG(DEBUG_NATIVES, printf("\n"));

    /* Validate arg list */
    currentArg = argList;
    if (IS_SYMBOL(currentArg)){
        /* Variable argument list */
    } else {
        while (!IS_NIL(currentArg)) {
            if (!IS_CONS(currentArg) || !IS_SYMBOL(CAR_OF(currentArg))) {
                SP_DEC(argsCount);
                errorf("Native lambda function arguments must be of SYMBOL type, not the %s type", TAG_NAME(TAG_OF(currentArg)));
            }
            currentArg = CDR_OF(currentArg);
        }
    }

    /* Create list of function bodies */
    bodyList = SCM_NIL_OBJ;
    for (int i = 1; i < argsCount; ++i) {
        bodyList = makeCons(SP_GET_AT(-i), bodyList); /* Body list in form of CONS: (body1 (body2 (... (bodyLast nil)...))) */
    }
    SP_DEC(argsCount);

    return makeUserFunc(argList, bodyList, env);
}


ScmObjPtr nativeIf(int argsCount, ScmEnvPtr env) {
    ScmObjPtr cond, trueExpr, falseExpr, rsltExpr;
    /*
     * Syntax:
     * (if cond trueExpr falseExpr)
     * (if (> a b) a b)
     *
     * Evaluates only and only one of true/false expression.
     * */
    if (argsCount != 3) {
        SP_DEC(argsCount);
        error("Native 'if' expects 3 arguments: (if <cond> <true-expression> <false-expression>)");
    }

    /* args are on the stack in reversed order */
    falseExpr = POP();
    trueExpr = POP();
    cond = POP();

    /* Evaluate condition */
    TMP_PUSH2(falseExpr, trueExpr);
    cond = eval(cond, env);
    TMP_POP2();
    if (IS_TRUE(cond)) {
        rsltExpr = eval(trueExpr, env);
    } else if (IS_FALSE(cond)) {
        rsltExpr = eval(falseExpr, env);
    } else {
        errora("Native 'if' condition must result in #true or #false, but found: ", cond);
        return NULL; /* Make compiler happy */
    }
    return rsltExpr;
}

ScmObjPtr nativeCond(int argsCount, ScmEnvPtr env) {
    ScmObjPtr caseObj, condPart, bodyPart, evaluatedCondPart,
            rsltObj = SCM_VOID_OBJ,
            bodiesToEvaluate = NULL;

    /*
     * Syntax:
     * (cond
     *      (<condA> <bodiesA>...)
     *      (<condB> <bodiesB>...)
     *      (<condC> <bodiesC>...)
     *      ...
     *      (else <bodiesElse>...)
     * )
     *
     */
    if (argsCount < 1) {
        SP_DEC(argsCount);
        error("Native 'cond' expects 1 argument at least: (cond (<condX> <bodyX>...)... (else <bodyElse>...))");
    }

    for (int i = argsCount; i > 0; i--) {
        caseObj = SP_GET_AT(-i);
        condPart = CAR_OF(caseObj);
        bodyPart = CDR_OF(caseObj);

        if (IS_SYMBOL(condPart) && OBJ_STR_EQ(condPart, "else")) {
            /* store else part for the future operations */
            bodiesToEvaluate = bodyPart;
        } else {
            /* cond part */
            evaluatedCondPart = eval(condPart, env);
            if (!IS_BOOLEAN(evaluatedCondPart)) {
                errora("Native 'cond' have to evaluate to boolean. But evaluated to: ", evaluatedCondPart);
            }
            if (IS_TRUE(evaluatedCondPart)) {
                /* condition matched - break and process body */
                bodiesToEvaluate = bodyPart;
                break;
            }
        }
    }

    /* Evaluate bodies */
    if (bodiesToEvaluate == NULL) {
        /* No expression matched */
        SP_DEC(argsCount);
        return SCM_VOID_OBJ;
    }
    while (!IS_NIL(bodiesToEvaluate)) {
        /* evaluate body */
        rsltObj = eval(CAR_OF(bodiesToEvaluate), env);

        /* move next body */
        bodiesToEvaluate = CDR_OF(bodiesToEvaluate);
    }

    SP_DEC(argsCount);

    return rsltObj;
}

ScmObjPtr nativeQuote(int argsCount, __attribute__((unused)) ScmEnvPtr env) {
    if (argsCount != 1) {
        SP_DEC(argsCount);
        error("Native 'quote' expects 1 argument");
    }
    return POP();
}


ScmObjPtr nativeEval(int argsCount, __attribute__((unused)) ScmEnvPtr env) {
    ScmObjPtr evaluatedArgument;
    ScmEnvPtr targetEnv = env;
    if (argsCount != 1) {
        SP_DEC(argsCount);
        error("Native 'eval' expects 1 argument");
    }

    /* get evaluated arg */
    ScmObjPtr exprPtr = POP();
    evaluatedArgument = eval(exprPtr, env);

    /* evaluate arg in target env */
    ScmObjPtr rsltPtr = eval(evaluatedArgument, targetEnv);

    return rsltPtr;
}


ScmObjPtr nativeTime(int argsCount, ScmEnvPtr env) {
    ScmObjPtr objPtr, evaluatedObjPtr;
    if (argsCount != 1) {
        SP_DEC(argsCount);
        error("Native 'time' needs exactly one argument (time (function-call-to-measure))");
    }

    objPtr = POP();

    /* Measure time */
    clock_t begin = clock();
    evaluatedObjPtr = eval(objPtr, env);
    clock_t end = clock();

    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

    /* printf("\n-> TIME: %f\n", time_spent); */
    ScmObjPtr rsltObj = makeCons(evaluatedObjPtr, SCM_NIL_OBJ);
    TMP_PUSH(rsltObj);
    ScmObjPtr timeObj = makeInteger((int)(time_spent * 1000000));
    TMP_POP();
    return makeCons(timeObj, rsltObj);
}
