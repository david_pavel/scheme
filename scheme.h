#ifndef __SCHEME_H__
#define __SCHEME_H__

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>


#ifndef bool
    typedef int bool;
    #define true 1
    #define false 0
#endif


/* ----------------- */
/* --- Debugging --- */
/* ----------------- */

#ifdef __DEBUG__
    #define DEBUG(flag, code) if ((flag)) (code)
#else
    #define DEBUG(flag, code) /* do nothing */
#endif


extern bool RESUME_ON_ERROR;

jmp_buf _jmpBuffer;

void AFTER_ERROR();

#define errorfa(format, scmObjPtr, ...) \
    fprintf(stderr,"ERROR: "); \
    fprintf(stderr, (format), __VA_ARGS__); \
    print((scmObjPtr), stderr); \
    fprintf(stderr,"\n"); \
    AFTER_ERROR();

#define errora(format, scmObjPtr) \
    fprintf(stderr,"ERROR: "); \
    fprintf(stderr, (format)); \
    print((scmObjPtr), stderr); \
    fprintf(stderr,"\n"); \
    AFTER_ERROR();

#define errorf(format, ...) \
    fprintf(stderr,"ERROR: "); \
    fprintf(stderr, (format), __VA_ARGS__); \
    fprintf(stderr,"\n"); \
    AFTER_ERROR();


#define error(msg) \
    fprintf(stderr, "ERROR: %s\n", msg); \
    AFTER_ERROR();

/* ------------------- */
/* --- Repl config --- */
/* ------------------- */

#define REPL_CONFIG int
#define REPL_DEFAULT_CONFIG                     0x0
#define REPL_CONFIG_SHOW_PROMPT                 0x1
#define REPL_CONFIG_PRINT_RESULT                0x2
#define REPL_CONFIG_PRINT_NEWLINE_AFTER_RESULT  0x4
#define REPL_CONFIG_SETJMP                      0x8
#define CONFIG_HAS(config, item) (((config) & (item)) != 0)

/* ------------------ */
/* --- Structures --- */
/* ------------------ */

enum TagEnum {
    TAG_INT,
    TAG_STRING,
    TAG_SYMBOL,
    TAG_TRUE,
    TAG_FALSE,
    TAG_NIL,
    TAG_VOID,
    TAG_EOF,
    TAG_CONS,
    TAG_NATIVE_FUNC,
    TAG_NATIVE_SYNTAX,
    TAG_USER_FUNC,
    TAG_ENV,
    TAG_ENV_ENTRY_ARRAY,
    TAG_FREE /* HEAP_FREE_MEMORY_CELL */
};
static const int STACK_INCREASE_FACTOR = 2;
typedef enum TagEnum TAG;

/* --- Object --- */

typedef union ScmObjUnion ScmObj;
typedef ScmObj* ScmObjPtr;

typedef struct ScmObjHdr ScmObjHdr;
typedef ScmObjHdr* ScmObjHdrPtr;
struct ScmObjHdr {
    TAG             tag;
    bool            marked;
    unsigned        size; /* Object size in bytes */
};

/* --- Environment --- */

struct EnvEntry {
    ScmObjPtr key;
    ScmObjPtr value;
};
typedef struct EnvEntry EnvEntry;

struct ScmEnvEntryArray {
    ScmObjHdr    header;
    unsigned int size;
    unsigned int count;
    EnvEntry     entries[1];
};
typedef struct ScmEnvEntryArray ScmEnvEntryArray;
typedef ScmEnvEntryArray* ScmEnvEntryArrayPtr;


typedef struct ScmEnv ScmEnv;
typedef ScmEnv* ScmEnvPtr;
struct ScmEnv {
    ScmObjHdr    header;
    ScmEnvPtr    parent;
    ScmEnvEntryArrayPtr entryArray;
};

/* --- Base structures --- */

struct ScmInt {
    ScmObjHdr header;
    int value;
};
typedef struct ScmInt ScmInt;
typedef ScmInt* ScmIntPtr;


struct ScmStr {
    ScmObjHdr header;
    char value[1];
};
typedef struct ScmStr ScmStr;
typedef ScmStr* ScmStrPtr;


struct ScmSym {
    ScmObjHdr header;
    char value[1];
};
typedef struct ScmSym ScmSym;
typedef ScmSym* ScmSymPtr;


struct ScmTrue {
    ScmObjHdr header;
    char value[1];
};
typedef struct ScmTrue ScmTrue;


struct ScmFalse {
    ScmObjHdr header;
    char value[1];
};
typedef struct ScmFalse ScmFalse;


struct ScmNil {
    ScmObjHdr header;
    char value[1];
};
typedef struct ScmNil ScmNil;


typedef ScmObjPtr (*FUNC_PTR)(int argsCount);

struct ScmNativeFunc {
    ScmObjHdr header;
    FUNC_PTR func;
};
typedef struct ScmNativeFunc ScmNativeFunc;
typedef ScmNativeFunc* ScmNativeFuncPtr;


typedef ScmObjPtr (*SYNTAX_PTR)(int argsCount, ScmEnvPtr env);

struct ScmNativeSyntax {
    ScmObjHdr header;
    SYNTAX_PTR func;
};
typedef struct ScmNativeSyntax ScmNativeSyntax;
typedef ScmNativeSyntax* ScmNativeSyntaxPtr;


struct ScmUserFunc {
    ScmObjHdr header;
    ScmObjPtr argList;
    ScmObjPtr bodyList;
    ScmEnvPtr parentEnv;
};
typedef struct ScmUserFunc ScmUserFunc;
typedef ScmUserFunc* ScmUserFuncPtr;


struct ScmCons {
    ScmObjHdr header;
    ScmObjPtr car;
    ScmObjPtr cdr;
};
typedef struct ScmCons ScmCons;
typedef ScmCons* ScmConsPtr;

struct ScmFree {
    ScmObjHdr header;
};
typedef struct ScmFree ScmFree;
typedef ScmFree* ScmFreePtr;


union ScmObjUnion {
    ScmObjHdr        header;
    ScmInt           integer;
    ScmStr           string;
    ScmSym           symbol;
    ScmCons          cons;
    ScmNativeFunc    nativeFunc;
    ScmNativeSyntax  nativeSyntax;
    ScmUserFunc      userFunc;
    ScmEnv           env;
    ScmEnvEntryArray envEntryArray;
    ScmFree          free;
};



/* ------------------------------ */
/* --- Constants declarations --- */
/* ------------------------------ */

extern ScmObjPtr SCM_TRUE_OBJ;
extern ScmObjPtr SCM_FALSE_OBJ;
extern ScmObjPtr SCM_NIL_OBJ;
extern ScmObjPtr SCM_VOID_OBJ;
extern ScmObjPtr SCM_EOF_OBJ;

extern ScmEnvPtr SCM_GLOBAL_ENV;

extern bool      SCM_EXIT;
extern bool      SCM_EXIT_CODE;


/* --------------------- */
/* --- Scheme config --- */
/* --------------------- */

typedef struct {
    /* heap */
    unsigned    heapChunkSize;
    unsigned    heapMaxChunksCount;

    /* stack */
    unsigned    stackInitSize;
    unsigned    stackMaxSize;

    /* other */
    bool        verbose;

    /* test */
    bool        runTest;
    bool        runBenchmark;

    /* errors */
    bool        resumeOnError;

    /* libs */
    char**      libraries;
    unsigned    librariesCnt;

    /* files */
    char**      files;
    unsigned    filesCnt;

    /* repl config */
    REPL_CONFIG replConfig;

} ScmConfig;
typedef ScmConfig* ScmConfigPtr;


#define TAG_OF(scmObjPtr) (((ScmObjPtr)(scmObjPtr))->header.tag)
#define CAR_OF(scmConsPtr) ((scmConsPtr)->cons.car)
#define CDR_OF(scmConsPtr) ((scmConsPtr)->cons.cdr)
#define INT_VALUE(scmConsPtr) ((scmConsPtr)->integer.value)
#define STR_VALUE(scmConsPtr) ((scmConsPtr)->string.value)
#define SYM_VALUE(scmConsPtr) ((scmConsPtr)->symbol.value)

#define IS_SYMBOL(scmObjPtr) (TAG_OF(scmObjPtr) == TAG_SYMBOL)
#define IS_NIL(scmObjPtr) (TAG_OF(scmObjPtr) == TAG_NIL)
#define IS_TRUE(scmObjPtr) (TAG_OF(scmObjPtr) == TAG_TRUE)
#define IS_FALSE(scmObjPtr) (TAG_OF(scmObjPtr) == TAG_FALSE)
#define IS_BOOLEAN(scmObjPtr) (IS_TRUE(scmObjPtr) || IS_FALSE(scmObjPtr))
#define IS_CONS(scmObjPtr) (TAG_OF(scmObjPtr) == TAG_CONS)
#define IS_INT(scmObjPtr) (TAG_OF(scmObjPtr) == TAG_INT)
#define IS_EOF(scmObjPtr) (TAG_OF(scmObjPtr) == TAG_EOF)
#define IS_VOID(scmObjPtr) (TAG_OF(scmObjPtr) == TAG_VOID)
#define IS_STRING(scmObjPtr) (TAG_OF(scmObjPtr) == TAG_STRING)
#define IS_USER_FUNC(scmObjPtr) (TAG_OF(scmObjPtr) == TAG_USER_FUNC)
#define IS_NATIVE_FUNC(scmObjPtr) (TAG_OF(scmObjPtr) == TAG_NATIVE_FUNC)
#define IS_NATIVE_SYNTAX(scmObjPtr) (TAG_OF(scmObjPtr) == TAG_NATIVE_SYNTAX)
#define IS_ENVIRONMENT(scmObjPtr) (TAG_OF(scmObjPtr) == TAG_ENV)

#define TO_SCM_BOOL(boolean) ((boolean) ? SCM_TRUE_OBJ : SCM_FALSE_OBJ)

#define OBJ_STR_EQ(scmObjPtr, str) ((strcmp((scmObjPtr)->string.value, (str))) == 0)
#define SYMBOLS_EQ(scmSymPtrA, scmSymPtrB) ((strcmp((scmSymPtrA)->symbol.value, (scmSymPtrB)->symbol.value)) == 0)


/* -------------------- */
/* --- Declarations --- */
/* -------------------- */

/* repl.c */
void repl(FILE *in, FILE *out, ScmEnvPtr env, REPL_CONFIG config);
void file(char *fileName, ScmEnvPtr env, REPL_CONFIG config);

/* eval.c */
ScmObjPtr   eval(ScmObjPtr objPtr, ScmEnvPtr env);
ScmObjPtr   evalFile(ScmObjPtr objPtr, ScmEnvPtr env, REPL_CONFIG replConfig);

/* printer.c */
void        print(ScmObjPtr, FILE*);

/* reader.c */
ScmObjPtr   read(FILE*);

/* objects.c */
ScmObjPtr   makeInteger(int number);
ScmObjPtr   makeString(char* str);
ScmObjPtr   makeSymbol(char* sym);
ScmObjPtr   makeSingletonSymbol(TAG tag);
ScmObjPtr   makeCons(ScmObjPtr car, ScmObjPtr cdr);
ScmObjPtr   makeNativeFunc(FUNC_PTR func);
ScmObjPtr   makeNativeSyntax(SYNTAX_PTR func);
ScmObjPtr   makeUserFunc(ScmObjPtr argList, ScmObjPtr bodyList, ScmEnvPtr parentEnv);

/* environment.c */
ScmEnvPtr   makeEnv(ScmEnvPtr parentEnvPtr);
ScmEnvPtr   makeEnvWithSize(ScmEnvPtr parentEnvPtr, unsigned defaultSize);
void        printEnvStdout(ScmEnvPtr envPtr);
void        printEnv(ScmEnvPtr envPtr, FILE* out);
bool        defineEnvValue(ScmEnvPtr envPtr, ScmObjPtr key, ScmObjPtr value);
bool        letEnvValue(ScmEnvPtr envPtr, ScmObjPtr key, ScmObjPtr value);
bool        setEnvValue(ScmEnvPtr envPtr, ScmObjPtr key, ScmObjPtr value);
ScmObjPtr   getEnvValue(ScmEnvPtr envPtr, ScmObjPtr key);

/* stack.h */
ScmObjPtr*  getStack(unsigned int *stackItemsCount);
void        printStack(FILE *out);
void        initStack(unsigned minStackSize, unsigned _maxStackSize);
void        destroyStack();
ScmObjPtr   POP();
ScmObjPtr   PUSH(ScmObjPtr objPtr);
ScmObjPtr   SP_GET_AT(int positionAgainstSP);
void        SP_SET_AT(int positionAgainstSP, ScmObjPtr objPtr);
int         SP_GET();
void        SP_DEC(int decreaseSize);

/* temp_stack.c */
void        initTempStack(unsigned tempStackSize);
void        destroyTempStack();
ScmObjPtr   TEMP_POP();
ScmObjPtr   TEMP_PUSH(ScmObjPtr objPtr);
ScmObjPtr*  getTempStack(unsigned int *stackItemsCount);
void        printTempStack(FILE *out);

/* natives.c */
ScmObjPtr   nativePlus(int evaluatedArgsCount);
ScmObjPtr   nativeProduct(int evaluatedArgsCount);
ScmObjPtr   nativeMinus(int evaluatedArgsCount);
ScmObjPtr   nativeDiv(int evaluatedArgsCount);
ScmObjPtr   nativeMod(int evaluatedArgsCount);
ScmObjPtr   nativeSqrt(int evaluatedArgsCount);
ScmObjPtr   nativeLoad(int evaluatedArgsCount);
ScmObjPtr   nativeReadNumber(int evaluatedArgsCount);
ScmObjPtr   nativeGreaterThen(int evaluatedArgsCount);
ScmObjPtr   nativeEqualsInt(int evaluatedArgsCount);
ScmObjPtr   nativeEqual(int evaluatedArgsCount);
ScmObjPtr   nativeIsBoolean(int evaluatedArgsCount);
ScmObjPtr   nativeIsEnv(int evaluatedArgsCount);
ScmObjPtr   nativeIsNil(int evaluatedArgsCount);
ScmObjPtr   nativeIsNumber(int evaluatedArgsCount);
ScmObjPtr   nativeIsProcedure(int evaluatedArgsCount);
ScmObjPtr   nativeIsString(int evaluatedArgsCount);
ScmObjPtr   nativeIsSymbol(int evaluatedArgsCount);
ScmObjPtr   nativeIsVoid(int evaluatedArgsCount);
ScmObjPtr   nativeCar(int evaluatedArgsCount);
ScmObjPtr   nativeCdr(int evaluatedArgsCount);
ScmObjPtr   nativeCons(int evaluatedArgsCount);
ScmObjPtr   nativeDisplay(int evaluatedArgsCount);
ScmObjPtr   nativeNewline(int evaluatedArgsCount);
ScmObjPtr   nativeList(int evaluatedArgsCount);
ScmObjPtr   nativeExit(int evaluatedArgsCount);
ScmObjPtr   nativeRandom(int evaluatedArgsCount);

ScmObjPtr   nativeDefine(int argsCount, ScmEnvPtr env);
ScmObjPtr   nativeSetEx(int argsCount, ScmEnvPtr env);
ScmObjPtr   nativeLet(int argsCount, ScmEnvPtr env);
ScmObjPtr   nativeLambda(int argsCount, ScmEnvPtr env);
ScmObjPtr   nativePrintEnv(int evaluatedArgsCount, ScmEnvPtr env);
ScmObjPtr   nativePrintHeap(int evaluatedArgsCount, ScmEnvPtr env);
ScmObjPtr   nativePrintStack(int evaluatedArgsCount, ScmEnvPtr env);
ScmObjPtr   nativeGc(int evaluatedArgsCount);
ScmObjPtr   nativeIf(int argsCount, ScmEnvPtr env);
ScmObjPtr   nativeCond(int argsCount, ScmEnvPtr env);
ScmObjPtr   nativeQuote(int argsCount, ScmEnvPtr env);
ScmObjPtr   nativeEval(int argsCount, ScmEnvPtr env);
ScmObjPtr   nativeTime(int argsCount, ScmEnvPtr env);

/* test.c */
void        runTests();

/* config.c */
ScmConfigPtr defaultScmConfig();
void         destroyScmConfig(ScmConfigPtr configPtr);
ScmConfigPtr parseScmConfig(int argc, char **argv, ScmConfigPtr defaultConfig);
void         printConfig(ScmConfigPtr configPtr);

/* init.c */
void        initSingletonObjects();
void        initScm(ScmConfigPtr scmConfig);
void        destroyScm();
void        initEnvNatives(ScmEnvPtr env);

/* helper.c */
char*       TAG_NAME(TAG tag);
#define     TAG_NAME_OF(objPtr) (TAG_NAME(TAG_OF((ScmObjPtr)objPtr)))
void        printHumanReadableSize(unsigned long size, FILE *out);

/* heap.c */
#define     POINTER unsigned char *
void        initHeap(unsigned chunkSize, unsigned maxChunksCount);
void        destroyHeap();
void        resetHeapCurrentFreeCell();
void        printHeap(FILE* out);
unsigned    heapTotalAllocatedMemory();
POINTER     scmMalloc(unsigned objectSize, TAG tag);

typedef     struct {
    POINTER     currentCell;
    unsigned   *currentChunkMemory;
    unsigned    currentChunkIndex;
} HeapIterator;
HeapIterator* heapIteratorCreate();
void          heapIteratorDestroy(HeapIterator* iterator);
void          heapIteratorReset(HeapIterator* iterator);
bool          heapIteratorNext(HeapIterator* iterator);

/* gc.c */
void        scmGC();
void        sweep();

/** Pop the object from the temporary roots */
#define TMP_POP() (POP())
#define TMP_POP2() TMP_POP();TMP_POP()
#define TMP_POP3() TMP_POP2();TMP_POP()
#define TMP_POP4() TMP_POP3();TMP_POP()

/** Push the object into the temporary roots */
#define TMP_PUSH(objPtr) (PUSH((ScmObjPtr)(objPtr)))
#define TMP_PUSH2(objPtr1, objPtr2) TMP_PUSH(objPtr1);TMP_PUSH(objPtr2)
#define TMP_PUSH3(objPtr1, objPtr2, objPtr3) TMP_PUSH(objPtr1);TMP_PUSH(objPtr2);TMP_PUSH(objPtr3)
#define TMP_PUSH4(objPtr1, objPtr2, objPtr3, objPtr4) TMP_PUSH(objPtr1);TMP_PUSH(objPtr2);TMP_PUSH(objPtr3);TMP_PUSH(objPtr4)

#endif