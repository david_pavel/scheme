#include <string.h>
#include "scheme.h"

static int testFailsCounter = 0;

#define ASSERTF(cond, message, ...) \
    if (!(cond)) { \
        testFailsCounter++; \
        fprintf(stdout, "!!\t\t[FAIL] "); \
        fprintf(stdout, message,  __VA_ARGS__); \
        fprintf(stdout, "\n"); \
    }

#define ASSERT(cond, message) \
    if (!(cond)) { \
        testFailsCounter++; \
        fprintf(stdout, "!!\t\t[FAIL] "); \
        fprintf(stdout, message); \
        fprintf(stdout, "\n"); \
    }

#define TEST_GROUP(name) \
    fprintf(stdout, "%s\n", (name));

#define TEST_CASE(name) \
    fprintf(stdout, "  %s\n", (name));

#define TEST_SUB_CASE(name) \
    fprintf(stdout, "    %s\n", (name));


static bool OBJ_EQUAL(ScmObjPtr objA, ScmObjPtr objB);
static bool LIST_EQUAL(ScmObjPtr listA, ScmObjPtr listB);

static bool LIST_EQUAL(ScmObjPtr listA, ScmObjPtr listB) {
    for(;;) {
        /* Compare car */
        if (!OBJ_EQUAL(CAR_OF(listA), CAR_OF(listA))) {
            return false;
        }

        /* Compare cdr */
        if (IS_NIL(CDR_OF(listA)) && IS_NIL(CDR_OF(listB))) {
            /* lists equal */
            return true;
        } else if (!IS_NIL(CDR_OF(listA)) && !IS_NIL(CDR_OF(listB))) {
            return LIST_EQUAL(CDR_OF(listA), CDR_OF(listB));
        } else {
            /* different length */
            return false;
        }
    }
}

static bool OBJ_EQUAL(ScmObjPtr objA, ScmObjPtr objB) {
    if (TAG_OF(objA) != TAG_OF(objB)) {
        return false;
    }
    switch (TAG_OF(objA)) {
        case TAG_NIL:
        case TAG_TRUE:
        case TAG_FALSE:
        case TAG_VOID:
        case TAG_EOF:
            return true;
        case TAG_INT:
            return INT_VALUE(objA) == INT_VALUE(objB);
        case TAG_SYMBOL:
        case TAG_STRING:
            return SYMBOLS_EQ(objA, objB);
        case TAG_CONS:
            return LIST_EQUAL(objA, objB);
        case TAG_USER_FUNC:
            return LIST_EQUAL(objA->userFunc.argList, objB->userFunc.argList)
                && LIST_EQUAL(objA->userFunc.bodyList, objB->userFunc.bodyList);
        case TAG_NATIVE_FUNC:
        case TAG_NATIVE_SYNTAX:
            return objA->nativeFunc.func == objB->nativeFunc.func;
        default:
            errorf("OBJ_EQUAL: unknown tag to compare: %d", TAG_OF(objA));
            return false; /* make compiler happy */
    }
}

static FILE* FILE_FROM_STR(char *str) {
    FILE *pFile;
    pFile = tmpfile();
    fputs(str, pFile);
    rewind(pFile);
    return pFile;
}

/* ------------------- */
/* --- Tests cases --- */
/* ------------------- */



void testReader() {
TEST_GROUP("Test Reader");

    TEST_CASE("read(FILE* input)");
    FILE *in = FILE_FROM_STR("(define (fac n)\n"
                                     "    (if (= n 1)\n"
                                     "        1\n"
                                     "        (* (fac (- n 1)) n)\n"
                                     "    )\n"
                                     ")\n");
    ScmObjPtr objPtr = read(in);
    ASSERT(objPtr->header.tag == TAG_CONS, "Should be cons");
    ASSERT(objPtr->cons.car->header.tag == TAG_SYMBOL, "Should be symbol");
    ASSERT(strcmp(objPtr->cons.car->symbol.value, "define") == 0, "First list argument shoul be 'define'");

    fclose(in);
}

void testEval() {
TEST_GROUP("Test Eval");

    initStack(10, 10);

    TEST_CASE("eval symbol");
    ScmEnvPtr env = makeEnv(NULL);
    defineEnvValue(env, makeSymbol("two"), makeInteger(2));
    defineEnvValue(env, makeSymbol("ten"), makeInteger(10));
    defineEnvValue(env, makeSymbol("false"), SCM_FALSE_OBJ);
    defineEnvValue(env, makeSymbol("hello"), makeString("world"));

    ScmObjPtr twoEvaluated = eval(makeSymbol("two"), env);
    ASSERT(IS_INT(twoEvaluated), "should be evaluated to integer");
    ASSERT(twoEvaluated->integer.value == 2, "should be evaluated to 2");

    ScmObjPtr tenEvaluated = eval(makeSymbol("ten"), env);
    ASSERT(IS_INT(tenEvaluated), "should be evaluated to integer");
    ASSERT(tenEvaluated->integer.value == 10, "should be evaluated to 10");

    ScmObjPtr falseEvaluated = eval(makeSymbol("false"), env);
    ASSERT(IS_FALSE(falseEvaluated), "should be evaluated to false");

    ScmObjPtr helloEvaluated = eval(makeSymbol("hello"), env);
    ASSERT(IS_STRING(helloEvaluated), "should be evaluated to string");
    ASSERT(strcmp(helloEvaluated->string.value, "world") == 0, "should be evaluated to 'world'");

    destroyStack();


TEST_CASE("eval native functions");

    /* Stack required for PUSH and POP, Environment for eval */

    initStack(10, 10);
    env = makeEnv(NULL);
    defineEnvValue(env, makeSymbol("+"), makeNativeFunc(nativePlus));

    ScmObjPtr listObj = makeCons(makeSymbol("+"), makeCons(makeInteger(1), makeCons(makeInteger(2), makeSingletonSymbol(TAG_NIL))));
    ScmObjPtr listObjEvaluated = eval(listObj, env);
    ASSERT(IS_INT(listObjEvaluated), "should be evaluated to int");
    ASSERT(INT_VALUE(listObjEvaluated) == 3, "should be evaluated to 3");


    /* Stack required for PUSH and POP */
    destroyStack();


}

void testEnvironment() {
    ScmEnvPtr env, parentEnv;
    ScmObjPtr value, key, parentValue;
TEST_GROUP("Test environment");

    TEST_CASE("makeEnv(NULL)");
    env = makeEnv(NULL);

    ASSERT(env->entryArray->count == 0, "Env count after initialization should be 0.");
    ASSERTF(env->entryArray->size > 0, "Env size must be grater then 0 after initialization, not %d", env->entryArray->size);


    TEST_CASE("defineEnvValue(...)");
    key = makeSymbol("numberSix");
    value = makeInteger(6);
    bool result = defineEnvValue(env, key, value);

    ASSERT(result == true, "Env set should success");
    ASSERT(env->entryArray->entries[0].key == key, "Env does not contain key");
    ASSERT(env->entryArray->entries[0].value == value, "Env does not contain value");

    TEST_CASE("getEnvValue(...)");
    ScmObjPtr returnedValue = getEnvValue(env, key);

    ASSERT(returnedValue == value, "Env getEnvValue failed");




    TEST_CASE("letEnvValue(...)");
    parentEnv = makeEnv(NULL);
    env = makeEnv(parentEnv);
    key = makeSymbol("a");

    parentValue = makeInteger(-10);
    defineEnvValue(parentEnv, key, parentValue);

    value = makeInteger(5);
    letEnvValue(env, key, value);

    ASSERT(parentEnv->entryArray->entries[0].key == key, "Parent env does not contain key");
    ASSERT(parentEnv->entryArray->entries[0].value == parentValue, "Parent env does not contain right value");

    ASSERT(env->entryArray->entries[0].key == key, "Env does not contain key");
    ASSERT(env->entryArray->entries[0].value == value, "Env does not contain right value");


    /* ---- */

    TEST_CASE("increaseEnvSize(...)");
    unsigned i, ENV_DEFAULT_SIZE = 4;
    char* randStr = malloc(2);
    randStr[0] = 'a';
    randStr[1] = '\0';

    env = makeEnvWithSize(NULL, ENV_DEFAULT_SIZE);

    for (i = 0 ; i < ENV_DEFAULT_SIZE; i++) {
        randStr[0] += 1;

        defineEnvValue(env, makeSymbol(randStr), makeInteger(i));
        ASSERTF(env->entryArray->size == ENV_DEFAULT_SIZE, "Env should not increase size after %d items inserted", i+1);
    }
    randStr[0] += 1;
    defineEnvValue(env, makeSymbol(randStr), makeInteger(i));
    ASSERTF(env->entryArray->size > ENV_DEFAULT_SIZE, "Env should increase size after %d items inserted", i+1);

    free(randStr);


TEST_CASE("get/set value in parent env");
    parentEnv = makeEnv(NULL);
    env = makeEnv(parentEnv);

    defineEnvValue(parentEnv, makeSymbol("symInParent"), makeString("parent"));
    defineEnvValue(parentEnv, makeSymbol("symInBoth"), makeString("parent"));
    defineEnvValue(env, makeSymbol("symInBoth"), makeString("child"));
    defineEnvValue(env, makeSymbol("symInChild"), makeString("child"));

    ScmObjPtr childObj = getEnvValue(env, makeSymbol("symInChild"));
    ScmObjPtr parentObj = getEnvValue(env, makeSymbol("symInParent"));
    ScmObjPtr child2Obj = getEnvValue(env, makeSymbol("symInBoth"));

    ScmObjPtr noObj = getEnvValue(parentEnv, makeSymbol("symInChild"));


    ASSERT(childObj != NULL, "Value for 'symInChild' should exist in environment");
    ASSERT(OBJ_STR_EQ(childObj, "child"), "Value should be found in child env");

    ASSERT(parentObj != NULL, "Value for 'symInParent' should exist in environment");
    ASSERT(OBJ_STR_EQ(parentObj, "parent"), "Value should be found in parent env");

    ASSERT(child2Obj != NULL, "Value for 'symInBoth' should exist in environment");
    ASSERT(OBJ_STR_EQ(child2Obj, "child"), "Value should be found in child env. Not form parent");

    ASSERT(noObj == NULL, "Value for 'symInBoth' should NOT exist in environment");





TEST_CASE("rewrite value in parent env");
    parentEnv = makeEnv(NULL);
    env = makeEnv(parentEnv);
    /* Preset value in parent */
    defineEnvValue(parentEnv, makeSymbol("p"), makeString("old-value"));
    /* Override value in parent form local env*/
    defineEnvValue(env, makeSymbol("p"), makeString("new-value"));

    TEST_SUB_CASE("parent has overriden value");
    ScmObjPtr objPtr = getEnvValue(parentEnv, makeSymbol("p"));
    ASSERT(IS_STRING(objPtr), "Should return string");
    ASSERT(OBJ_STR_EQ(objPtr, "new-value"), "Should return overriden value");

    TEST_SUB_CASE("env transitivly has parent's overriden value");
    objPtr = getEnvValue(env, makeSymbol("p"));
    ASSERT(IS_STRING(objPtr), "Should return string");
    ASSERT(OBJ_STR_EQ(objPtr, "new-value"), "Should return overriden value from parent");

    TEST_SUB_CASE("top env it self does NOT have value");
    env->parent = NULL;
    objPtr = getEnvValue(env, makeSymbol("p"));
    ASSERT(objPtr == NULL, "Should not found in top env");



}

void testDefinitions() {
TEST_GROUP("Test definitions");

    ScmObjPtr objPtr;

    TEST_CASE("TAG_OF(scmObj))");
    objPtr = makeSymbol("symbol");
    ASSERT(TAG_OF(objPtr) == TAG_SYMBOL, "should be TAG_SYMBOL");


    TEST_CASE("IS_SYMBOL(scmObj))");
    objPtr = makeSymbol("symbol");
    ASSERT(IS_SYMBOL(objPtr), "should be TAG_SYMBOL");


    TEST_CASE("IS_TRUE(scmObj))");
    objPtr = SCM_TRUE_OBJ;
    ASSERT(IS_TRUE(objPtr), "should be TAG_TRUE");


    TEST_CASE("IS_FALSE(scmObj))");
    objPtr = SCM_FALSE_OBJ;
    ASSERT(IS_FALSE(objPtr), "should be TAG_FALSE");


    TEST_CASE("IS_NIL(scmObj))");
    objPtr = SCM_NIL_OBJ;
    ASSERT(IS_NIL(objPtr), "should be TAG_NIL");


    TEST_CASE("IS_CONS(scmObj))");
    objPtr = makeCons(SCM_TRUE_OBJ, SCM_NIL_OBJ);
    ASSERT(IS_CONS(objPtr), "should be TAG_CONS");

    TEST_CASE("IS_INT(scmObj))");
    objPtr = makeInteger(100);
    ASSERT(IS_INT(objPtr), "should be TAG_INT");


    TEST_CASE("IS_STRING(scmObj))");
    objPtr = makeString("This is my scheme!");
    ASSERT(IS_STRING(objPtr), "should be TAG_STRING");


    TEST_CASE("STR_VALUE(scmObj))");
    objPtr = makeString("This is my scheme!");
    char* strValue = STR_VALUE(objPtr);
    ASSERT(strcmp(strValue,"This is my scheme!") == 0, "fail");


    TEST_CASE("SYM_VALUE(scmObj))");
    objPtr = makeSymbol("This is my scheme!");
    char* symValue = SYM_VALUE(objPtr);
    ASSERT(strcmp(symValue,"This is my scheme!") == 0, "fail");

}

void testNativeFunctions() {
TEST_GROUP("Test native functions");
    ScmObjPtr objPtr, rsltPtr, expectedObj, consPtr;
    ScmEnvPtr env;
    int stackPointerBefore, stackPointerAfter;
    FILE* tmpfile;

    /* Stack required for PUSH and POP */
    initStack(100, 100);

TEST_CASE("(*)");

    TEST_SUB_CASE("nativeProduct (*)");
    objPtr = nativeProduct(0);
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == 1, "multiplication failed");

    TEST_SUB_CASE("nativeProduct (* 6)");
    PUSH(makeInteger(6));
    objPtr = nativeProduct(1);
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == 6, "product failed");

    TEST_SUB_CASE("nativeProduct (* 3 2)");
    PUSH(makeInteger(3));
    PUSH(makeInteger(2));
    objPtr = nativeProduct(2);
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == 6, "product failed");

    TEST_SUB_CASE("nativeProduct (* 1 2 3 4)");
    PUSH(makeInteger(1));
    PUSH(makeInteger(2));
    PUSH(makeInteger(3));
    PUSH(makeInteger(4));
    objPtr = nativeProduct(4);
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == 24, "product failed");

TEST_CASE("(+)");

    TEST_SUB_CASE("nativePlus (+)");
    objPtr = nativePlus(0);
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == 0, "plus failed");


    TEST_SUB_CASE("nativePlus (+ 1 2)");
    PUSH(makeInteger(1));
    PUSH(makeInteger(2));
    objPtr = nativePlus(2);
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == 3, "plus failed");


    TEST_SUB_CASE("nativePlus (+ 732)");
    PUSH(makeInteger(732));
    objPtr = nativePlus(1);
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == 732, "plus failed");


    TEST_SUB_CASE("nativePlus (+ 10 20 30 40)");
    stackPointerBefore = SP_GET();
    PUSH(makeInteger(10));
    PUSH(makeInteger(20));
    PUSH(makeInteger(30));
    PUSH(makeInteger(40));
    objPtr = nativePlus(4);
    stackPointerAfter = SP_GET();
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == 100, "plus failed");


    TEST_SUB_CASE("nativePlus - stack clearing");
    ASSERT(stackPointerBefore == stackPointerAfter, "stack not cleared after operations");


TEST_CASE("(-)");

    TEST_SUB_CASE("nativeMinus (- 3)");
    PUSH(makeInteger(3));
    objPtr = nativeMinus(1);
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == -3, "failed");


    TEST_SUB_CASE("nativeMinus (- 3 4)");
    PUSH(makeInteger(3));
    PUSH(makeInteger(4));
    objPtr = nativeMinus(2);
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == -1, "failed");


    TEST_SUB_CASE("nativeMinus (- 100 50 30 20)");
    stackPointerBefore = SP_GET();
    PUSH(makeInteger(100));
    PUSH(makeInteger(50));
    PUSH(makeInteger(30));
    PUSH(makeInteger(20));
    objPtr = nativeMinus(4);
    stackPointerAfter = SP_GET();
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == 0, "failed");
    ASSERT(INT_VALUE(objPtr) == 0, "failed");


    TEST_SUB_CASE("nativeMinus - stack clearing");
    ASSERT(stackPointerBefore == stackPointerAfter, "stack not cleared after operations");


TEST_CASE("(/)");


    TEST_SUB_CASE("nativeDiv (/ 8 2)");
    PUSH(makeInteger(8));
    PUSH(makeInteger(2));
    objPtr = nativeDiv(2);
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == 4, "failed");


    TEST_SUB_CASE("nativeDiv (/ 100 50 30 20)");
    stackPointerBefore = SP_GET();
    PUSH(makeInteger(1000));
    PUSH(makeInteger(2));
    PUSH(makeInteger(5));
    PUSH(makeInteger(10));
    objPtr = nativeDiv(4);
    stackPointerAfter = SP_GET();
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == 10, "failed");


    TEST_SUB_CASE("nativeDiv - stack clearing");
    ASSERT(stackPointerBefore == stackPointerAfter, "stack not cleared after operations");


TEST_CASE("%, mod");

    TEST_SUB_CASE("nativeMod (mod 3 2)");
    PUSH(makeInteger(3));
    PUSH(makeInteger(2));
    objPtr = nativeMod(2);
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == 1, "failed");


    TEST_SUB_CASE("nativeMod (mod 4 2)");
    stackPointerBefore = SP_GET();
    PUSH(makeInteger(4));
    PUSH(makeInteger(2));
    objPtr = nativeMod(2);
    stackPointerAfter = SP_GET();
    ASSERT(IS_INT(objPtr), "should be TAG_INT");
    ASSERT(INT_VALUE(objPtr) == 0, "failed");


    TEST_SUB_CASE("nativeMod - stack clearing");
    ASSERT(stackPointerBefore == stackPointerAfter, "stack not cleared after operations");


TEST_CASE("(> a b)");

    TEST_SUB_CASE("nativeGreaterThen (> 3 2)");
    PUSH(makeInteger(3));
    PUSH(makeInteger(2));
    objPtr = nativeGreaterThen(2);
    ASSERT(IS_TRUE(objPtr), "should be #true");


    TEST_SUB_CASE("nativeGreaterThen (> 2 3)");
    PUSH(makeInteger(2));
    PUSH(makeInteger(3));
    objPtr = nativeGreaterThen(2);
    ASSERT(IS_FALSE(objPtr), "should be #false");


    TEST_SUB_CASE("nativeGreaterThen (> -1 -5)");
    PUSH(makeInteger(-1));
    PUSH(makeInteger(-5));
    objPtr = nativeGreaterThen(2);
    ASSERT(IS_TRUE(objPtr), "should be #true");


    TEST_SUB_CASE("nativeGreaterThen (> 3 -5)");
    PUSH(makeInteger(3));
    PUSH(makeInteger(-5));
    objPtr = nativeGreaterThen(2);
    ASSERT(IS_TRUE(objPtr), "should be #true");


    TEST_SUB_CASE("nativeGreaterThen (> 0 -0)");
    PUSH(makeInteger(0));
    PUSH(makeInteger(-0));
    objPtr = nativeGreaterThen(2);
    ASSERT(IS_FALSE(objPtr), "should be #false");


    TEST_SUB_CASE("nativeGreaterThen (> 0 -0)");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(> (+ 1 2 3) (- 8 5 (+ 1 6)))")); fclose(tmpfile);
    objPtr = eval(objPtr, env);

    ASSERT(IS_TRUE(objPtr), "should be #true");


    TEST_SUB_CASE("nativeGreaterThen - stack clearing");
    stackPointerBefore = SP_GET();
    PUSH(makeInteger(0));
    PUSH(makeInteger(-0));
    nativeGreaterThen(2);
    stackPointerAfter = SP_GET();
    ASSERT(stackPointerBefore == stackPointerAfter, "stack not cleared after operations");


TEST_CASE("=");

    TEST_SUB_CASE("(= 1 1)");
    PUSH(makeInteger(1));
    PUSH(makeInteger(1));
    objPtr = nativeEqualsInt(2);
    ASSERT(IS_TRUE(objPtr), "should be #true");


    TEST_SUB_CASE("(= 1 2)");
    PUSH(makeInteger(1));
    PUSH(makeInteger(2));
    objPtr = nativeEqualsInt(2);
    ASSERT(IS_FALSE(objPtr), "should be #false");


    TEST_SUB_CASE("(= 2 1)");
    PUSH(makeInteger(2));
    PUSH(makeInteger(1));
    objPtr = nativeEqualsInt(2);
    ASSERT(IS_FALSE(objPtr), "should be #false");


    TEST_SUB_CASE("(= -22 -22)");
    PUSH(makeInteger(-22));
    PUSH(makeInteger(-22));
    objPtr = nativeEqualsInt(2);
    ASSERT(IS_TRUE(objPtr), "should be #true");


    TEST_SUB_CASE("(= (-2 -1)");
    PUSH(makeInteger(-2));
    PUSH(makeInteger(-1));
    objPtr = nativeEqualsInt(2);
    ASSERT(IS_FALSE(objPtr), "should be #false");


    TEST_SUB_CASE("(= (-1 -2)");
    PUSH(makeInteger(-1));
    PUSH(makeInteger(-2));
    objPtr = nativeEqualsInt(2);
    ASSERT(IS_FALSE(objPtr), "should be #false");


    TEST_SUB_CASE("(= (-1 2)");
    PUSH(makeInteger(-1));
    PUSH(makeInteger(2));
    objPtr = nativeEqualsInt(2);
    ASSERT(IS_FALSE(objPtr), "should be #false");


    TEST_SUB_CASE("(= (1 -2)");
    PUSH(makeInteger(1));
    PUSH(makeInteger(-2));
    objPtr = nativeEqualsInt(2);
    ASSERT(IS_FALSE(objPtr), "should be #false");

    TEST_SUB_CASE("(= 0 0)");
stackPointerBefore = SP_GET();
    PUSH(makeInteger(0));
    PUSH(makeInteger(0));
    objPtr = nativeEqualsInt(2);
stackPointerAfter = SP_GET();
    ASSERT(IS_TRUE(objPtr), "should be #true");


    TEST_SUB_CASE("'=' - stack clearing");
    ASSERT(stackPointerBefore == stackPointerAfter, "stack not cleared after operations");


TEST_CASE("nil?");

    TEST_SUB_CASE("(?nil #nil)");
    PUSH(SCM_NIL_OBJ);
    objPtr = nativeIsNil(1);
    ASSERT(IS_TRUE(objPtr), "should be #true");


    TEST_SUB_CASE("(?nil #f)");
    PUSH(SCM_FALSE_OBJ);
    objPtr = nativeIsNil(1);
    ASSERT(IS_FALSE(objPtr), "should be #false");


    TEST_SUB_CASE("(?nil #t)");
    PUSH(SCM_TRUE_OBJ);
    objPtr = nativeIsNil(1);
    ASSERT(IS_FALSE(objPtr), "should be #false");


    TEST_SUB_CASE("(?nil \"Hello\")");
    PUSH(makeString("Hello"));
    objPtr = nativeIsNil(1);
    ASSERT(IS_FALSE(objPtr), "should be #false");


    TEST_SUB_CASE("(?nil #void)");
stackPointerBefore = SP_GET();
    PUSH(SCM_VOID_OBJ);
    objPtr = nativeIsNil(1);
stackPointerAfter = SP_GET();
    ASSERT(IS_FALSE(objPtr), "should be #false");

    TEST_SUB_CASE("'nil?' - stack clearing");
    ASSERT(stackPointerBefore == stackPointerAfter, "stack not cleared after operations");


    TEST_CASE("car");

    TEST_SUB_CASE("(car '(2 3 4))");
    objPtr = read(tmpfile = FILE_FROM_STR("(2 3 4)")); fclose(tmpfile);
    PUSH(objPtr);
    rsltPtr = nativeCar(1);
    ASSERT(IS_INT(rsltPtr), "should be integer");
    ASSERT(INT_VALUE(rsltPtr) == 2, "should be 2");


    TEST_SUB_CASE("(car '(4))");
    objPtr = read(tmpfile = FILE_FROM_STR("(4)")); fclose(tmpfile);
    stackPointerBefore = SP_GET();
    PUSH(objPtr);
    rsltPtr = nativeCar(1);
    stackPointerAfter = SP_GET();
    ASSERT(IS_INT(rsltPtr), "should be integer");
    ASSERT(INT_VALUE(rsltPtr) == 4, "should be 4");


    TEST_SUB_CASE("'car' - stack clearing");
    ASSERT(stackPointerBefore == stackPointerAfter, "stack not cleared after operations");

TEST_CASE("cdr");


    TEST_SUB_CASE("(cdr '(4))");
    objPtr = read(tmpfile = FILE_FROM_STR("(4)")); fclose(tmpfile);
    PUSH(objPtr);
    rsltPtr = nativeCdr(1);
    ASSERT(IS_NIL(rsltPtr), "should be NIL");

    TEST_SUB_CASE("(cdr '(2 3 4))");
    objPtr = read(tmpfile = FILE_FROM_STR("(2 3 4)")); fclose(tmpfile);
stackPointerBefore = SP_GET();
    PUSH(objPtr);
    rsltPtr = nativeCdr(1);
stackPointerAfter = SP_GET();
    ASSERT(IS_CONS(rsltPtr), "should be CONS");
    expectedObj = read(tmpfile = FILE_FROM_STR("(3 4)")); fclose(tmpfile);
    ASSERT(LIST_EQUAL(rsltPtr, expectedObj), "should be equal");

    TEST_SUB_CASE("'cdr' - stack clearing");
    ASSERT(stackPointerBefore == stackPointerAfter, "stack not cleared after operations");


TEST_CASE("cons");


    TEST_SUB_CASE("(cons 1 #nil)");
    PUSH(makeInteger(1));
    PUSH(SCM_NIL_OBJ);
    rsltPtr = nativeCons(2);
    ASSERT(IS_CONS(rsltPtr), "should be CONS");
    ASSERT(INT_VALUE(CAR_OF(rsltPtr)) == 1, "car should be 1");
    ASSERT(IS_NIL(CDR_OF(rsltPtr)), "cdr should be #nil");

    TEST_SUB_CASE("(cons 5 (cons 1 #nil))");
stackPointerBefore = SP_GET();
    PUSH(makeInteger(1));
    PUSH(SCM_NIL_OBJ);
    consPtr = nativeCons(2);
    PUSH(makeInteger(5));
    PUSH(consPtr);
    rsltPtr = nativeCons(2);
stackPointerAfter = SP_GET();
    ASSERT(IS_CONS(rsltPtr), "should be CONS");
    ASSERT(IS_CONS(CDR_OF(rsltPtr)), "cdr should be CONS");
    ASSERT(INT_VALUE(CAR_OF(rsltPtr)) == 5, "car should be 5");
    ASSERT(INT_VALUE(CAR_OF(CDR_OF(rsltPtr))) == 1, "car of cdr should be 1");
    ASSERT(IS_NIL(CDR_OF(CDR_OF(rsltPtr))), "cdr of cdr should be #nil");

    TEST_SUB_CASE("'cons' - stack clearing");
    ASSERT(stackPointerBefore == stackPointerAfter, "stack not cleared after operations");


TEST_CASE("list");

    TEST_SUB_CASE("(list)");
    rsltPtr = nativeList(0);
    ASSERT(IS_NIL(rsltPtr), "should be #nil");

    TEST_SUB_CASE("(list 1 2 3)");
    stackPointerBefore = SP_GET();
    PUSH(makeInteger(1));
    PUSH(makeInteger(2));
    PUSH(makeInteger(3));
    rsltPtr = nativeList(3);
    stackPointerAfter = SP_GET();
    ASSERT(IS_CONS(rsltPtr), "should be LIST");
    ASSERT(INT_VALUE(CAR_OF(rsltPtr)) == 1, "car should be 1");
    ASSERT(INT_VALUE(CAR_OF(CDR_OF(rsltPtr))) == 2, "car should be 2");
    ASSERT(INT_VALUE(CAR_OF(CDR_OF(CDR_OF(rsltPtr)))) == 3, "car should be 3");
    ASSERT(IS_NIL(CDR_OF(CDR_OF(CDR_OF(rsltPtr)))), "cdr should be #nil");

    TEST_SUB_CASE("'list' - stack clearing");
    ASSERT(stackPointerBefore == stackPointerAfter, "stack not cleared after operations");

    TEST_SUB_CASE("(list 1 (list 2 3) 4)");
    rsltPtr = eval(read(tmpfile = FILE_FROM_STR("(list 1 (list 2 3) 4)")), env); fclose(tmpfile);
    ASSERT(IS_CONS(rsltPtr), "should be LIST");
    ASSERT(INT_VALUE(CAR_OF(rsltPtr)) == 1, "car should be 1");
    ASSERT(INT_VALUE(CAR_OF(CAR_OF(CDR_OF(rsltPtr)))) == 2, "caadr should be 2");
    ASSERT(INT_VALUE(CAR_OF(CDR_OF(CAR_OF(CDR_OF(rsltPtr))))) == 3, "cadadr should be 3");
    ASSERT(IS_NIL(CDR_OF(CDR_OF(CAR_OF(CDR_OF(rsltPtr))))), "cddadr should be #nil");
    ASSERT(INT_VALUE(CAR_OF(CDR_OF(CDR_OF(rsltPtr)))) == 4, "cddar should be 4");
    ASSERT(IS_NIL(CDR_OF(CDR_OF(CDR_OF(rsltPtr)))), "cdr should be #nil");


    /* Stack required for PUSH and POP */
    destroyStack();
}

void testNativeSyntax() {
TEST_GROUP("Test native syntax");
    ScmObjPtr objPtr, rsltPtr, lambdaPtr;
    ScmEnvPtr env;
    int stackPointerBefore, stackPointerAfter;
    FILE* tmpfile;

    /* Stack required for PUSH and POP */
    initStack(100, 100);

TEST_CASE("define symbol");

    TEST_SUB_CASE("stack free");
    ASSERT(SP_GET() == 0,"Precondition failed: Stack not empty");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = makeCons(makeSymbol("define"), makeCons(makeSymbol("a"), makeCons(makeInteger(123), SCM_NIL_OBJ)));
    eval(objPtr, env);
    ASSERT(SP_GET() == 0,"Stack not freed");



    TEST_SUB_CASE("define int");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = makeCons(makeSymbol("define"), makeCons(makeSymbol("a"), makeCons(makeInteger(123), SCM_NIL_OBJ)));
    rsltPtr = eval(objPtr, env);
    ASSERT(IS_VOID(rsltPtr),"Should be #void");
    rsltPtr = getEnvValue(env, makeSymbol("a"));
    ASSERT(IS_INT(rsltPtr),"Result should be int");
    ASSERT(INT_VALUE(rsltPtr) == 123,"Result should be 123");


    TEST_SUB_CASE("define string");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(define x \"hello\")")); fclose(tmpfile);
    rsltPtr = eval(objPtr, env);
    ASSERT(IS_VOID(rsltPtr),"Should be #void");
    rsltPtr = getEnvValue(env, makeSymbol("x"));
    ASSERT(IS_STRING(rsltPtr),"Result should be string");
    ASSERT(OBJ_STR_EQ(rsltPtr,"hello"),"Result should be 'hello'");


    TEST_SUB_CASE("define expr");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(define x (+ 1 2 (- 3 0 6)))")); fclose(tmpfile);
    rsltPtr = eval(objPtr, env);
    ASSERT(IS_VOID(rsltPtr),"Should be #void");
    rsltPtr = getEnvValue(env, makeSymbol("x"));
    ASSERT(rsltPtr != NULL,"Result should exist");
    ASSERT(IS_INT(rsltPtr),"Result should be int");
    ASSERT(INT_VALUE(rsltPtr) == 0,"Result should be 0");


TEST_CASE("define lambda");

    TEST_SUB_CASE("define lambda - with args");
    env = makeEnv(NULL);
    initEnvNatives(env);
    rsltPtr = eval(read(tmpfile = FILE_FROM_STR("(define (max x y) (if (> x y) x y))")), env); fclose(tmpfile);
    ASSERT(IS_VOID(rsltPtr),"Should be #void");

    TEST_SUB_CASE("eval defined lambda (1)");
    rsltPtr = eval(read(tmpfile = FILE_FROM_STR("(max 3 5)")), env); fclose(tmpfile);
    ASSERT(IS_INT(rsltPtr),"Result should be int");
    ASSERT(INT_VALUE(rsltPtr) == 5,"Result should be 5");

    TEST_SUB_CASE("eval defined lambda (2)");
    rsltPtr = eval(read(tmpfile = FILE_FROM_STR("(max 3 (- 5))")), env); fclose(tmpfile);
    ASSERT(IS_INT(rsltPtr),"Result should be int");
    ASSERT(INT_VALUE(rsltPtr) == 3,"Result should be 5");

    TEST_SUB_CASE("define lambda - no args");
    rsltPtr = eval(read(tmpfile = FILE_FROM_STR("(define (number-100) 1 2 100)")), env); fclose(tmpfile);
    ASSERT(IS_VOID(rsltPtr),"Should be #void");

    TEST_SUB_CASE("eval defined no args lambda");
    rsltPtr = eval(read(tmpfile = FILE_FROM_STR("(number-100)")), env); fclose(tmpfile);
    ASSERT(IS_INT(rsltPtr),"Result should be int");
    ASSERT(INT_VALUE(rsltPtr) == 100,"Result should be 100");


TEST_CASE("lambda");

    TEST_SUB_CASE("stack free");
    ASSERT(SP_GET() == 0,"Precondition failed: Stack not empty");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(lambda () (+ 1 2))")); fclose(tmpfile);
    eval(objPtr, env);
    ASSERT(SP_GET() == 0,"Stack not freed");



    TEST_SUB_CASE("no args");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(lambda () (+ 1 2))")); fclose(tmpfile);
    lambdaPtr = eval(objPtr, env);
    ASSERT(IS_USER_FUNC(lambdaPtr),"Eval lambda should be <user-function>");



    TEST_SUB_CASE("no args multiple body");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(lambda () (+ 1 2) (- 10 5))")); fclose(tmpfile);
    lambdaPtr = eval(objPtr, env);
    ASSERT(IS_USER_FUNC(lambdaPtr),"Eval lambda should be <user-function>");



    TEST_SUB_CASE("one arg");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(lambda (a) (+ a 2))")); fclose(tmpfile);
    defineEnvValue(env, makeSymbol("a"), makeInteger(7));
    lambdaPtr = eval(objPtr, env);
    ASSERT(IS_USER_FUNC(lambdaPtr),"Eval lambda should be <user-function>");



    TEST_SUB_CASE("one arg multiple body");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(lambda (a) (- a 2) (+ a 2))")); fclose(tmpfile);
    defineEnvValue(env, makeSymbol("a"), makeInteger(7));
    lambdaPtr = eval(objPtr, env);
    ASSERT(IS_USER_FUNC(lambdaPtr),"Eval lambda should be <user-function>");



    TEST_SUB_CASE("multiple arg");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(lambda (a b) (+ a b))")); fclose(tmpfile);
    defineEnvValue(env, makeSymbol("a"), makeInteger(7));
    defineEnvValue(env, makeSymbol("b"), makeInteger(2));
    lambdaPtr = eval(objPtr, env);
    ASSERT(IS_USER_FUNC(lambdaPtr),"Eval lambda should be <user-function>");



    TEST_SUB_CASE("multiple arg multiple body");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(lambda (a b) (- a b) (+ a b))")); fclose(tmpfile);
    defineEnvValue(env, makeSymbol("a"), makeInteger(7));
    defineEnvValue(env, makeSymbol("b"), makeInteger(2));
    lambdaPtr = eval(objPtr, env);
    ASSERT(IS_USER_FUNC(lambdaPtr),"Eval lambda should be <user-function>");




TEST_CASE("if");

    TEST_SUB_CASE("#true");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(if #t 1 0)")); fclose(tmpfile);
    rsltPtr = eval(objPtr, env);
    ASSERT(IS_INT(rsltPtr),"Eval if should result in int");
    ASSERT(INT_VALUE(rsltPtr) == 1,"Eval if should be true expression");



    TEST_SUB_CASE("#false");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(if #f 1 0)"));
    rsltPtr = eval(objPtr, env);
    ASSERT(IS_INT(rsltPtr),"Eval if should result in int");
    ASSERT(INT_VALUE(rsltPtr) == 0,"Eval if should be false expression");
    fclose(tmpfile);



    TEST_SUB_CASE("(> 2 1)");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(if (> 2 1) 1 0)"));
    rsltPtr = eval(objPtr, env);
    ASSERT(IS_INT(rsltPtr),"Eval if should result in int");
    ASSERT(INT_VALUE(rsltPtr) == 1,"Eval if should be true expression");
    fclose(tmpfile);



    TEST_SUB_CASE("(> 1 2)");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(if (> 1 2) 1 0)"));
    rsltPtr = eval(objPtr, env);
    ASSERT(IS_INT(rsltPtr),"Eval if should result in int");
    ASSERT(INT_VALUE(rsltPtr) == 0,"Eval if should be false expression");
    fclose(tmpfile);



    TEST_SUB_CASE("(if (> a b) (- a b) (- b a)) ;a=8, b=5");
    env = makeEnv(NULL);
    initEnvNatives(env);
    defineEnvValue(env, makeSymbol("a"), makeInteger(8));
    defineEnvValue(env, makeSymbol("b"), makeInteger(5));
    objPtr = read(tmpfile = FILE_FROM_STR("(if (> a b) (- a b) (- b a))"));
    rsltPtr = eval(objPtr, env);
    ASSERT(IS_INT(rsltPtr),"Eval if should result in int");
    ASSERT(INT_VALUE(rsltPtr) == 3 ,"Eval if should be false expression");
    fclose(tmpfile);



    TEST_SUB_CASE("(if (> a b) (- a b) (- b a)) ;a=5, b=8 ");
    env = makeEnv(NULL);
    initEnvNatives(env);
    defineEnvValue(env, makeSymbol("a"), makeInteger(5));
    defineEnvValue(env, makeSymbol("b"), makeInteger(8));
    objPtr = read(tmpfile = FILE_FROM_STR("(if (> a b) (- a b) (- b a))"));
stackPointerBefore = SP_GET();
    rsltPtr = eval(objPtr, env);
stackPointerAfter = SP_GET();
    ASSERT(IS_INT(rsltPtr),"Eval if should result in int");
    ASSERT(INT_VALUE(rsltPtr) == 3 ,"Eval if should be false expression");
    fclose(tmpfile);



    TEST_SUB_CASE("if - stack clearing");
    ASSERT(stackPointerBefore == stackPointerAfter, "stack not cleared after operations");



TEST_CASE("quote");

    TEST_SUB_CASE("symbol quote read");
    read(tmpfile = FILE_FROM_STR("'a")); fclose(tmpfile);


    TEST_SUB_CASE("symbol quote eval");
    env = makeEnv(NULL);
    initEnvNatives(env);
    eval(read(tmpfile = FILE_FROM_STR("(define a 10)")), env); fclose(tmpfile);
    rsltPtr = eval(read(tmpfile = FILE_FROM_STR("'a")), env); fclose(tmpfile);
    ASSERT(IS_SYMBOL(rsltPtr),"Should be symbol");
    ASSERT(OBJ_STR_EQ(rsltPtr, "a"),"Should be symbol named 'a'");


    /* Stack required for PUSH and POP */
    destroyStack();
}

void testUserFunctions() {
    TEST_GROUP("Test user functions");
    ScmObjPtr objPtr, rsltPtr;
    ScmEnvPtr env;
    FILE* tmpfile;

    /* Stack required for PUSH and POP */
    initStack(100, 100);

TEST_CASE("lambda");


    TEST_SUB_CASE("no args");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(define sum (lambda () (+ 1 2)))")); fclose(tmpfile);
    eval(objPtr, env);
    objPtr = read(tmpfile = FILE_FROM_STR("(sum)")); fclose(tmpfile);
    rsltPtr = eval(objPtr, env);
    ASSERT(IS_INT(rsltPtr),"Result should be int");
    ASSERT(INT_VALUE(rsltPtr) == 3,"Result should be 3");



    TEST_SUB_CASE("no args multiple body");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(define sum (lambda () (+ 1 2) (- 10 5)))")); fclose(tmpfile);
    eval(objPtr, env);
    objPtr = read(tmpfile = FILE_FROM_STR("(sum)")); fclose(tmpfile);
    rsltPtr = eval(objPtr, env);
    ASSERT(IS_INT(rsltPtr),"Result should be int");
    ASSERT(INT_VALUE(rsltPtr) == 5,"Result should be 5");



    TEST_SUB_CASE("one arg");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(define sum (lambda (a) (+ a 2)))")); fclose(tmpfile);
    eval(objPtr, env);
    objPtr = read(tmpfile = FILE_FROM_STR("(sum 7)")); fclose(tmpfile);
    rsltPtr = eval(objPtr, env);
    ASSERT(IS_INT(rsltPtr),"Result should be int");
    ASSERT(INT_VALUE(rsltPtr) == 9,"Result should be 9");



    TEST_SUB_CASE("one arg multiple body");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(define sum (lambda (a) (- a 2) (+ a 2)))")); fclose(tmpfile);
    eval(objPtr, env);
    objPtr = read(tmpfile = FILE_FROM_STR("(sum 7)")); fclose(tmpfile);
    rsltPtr = eval(objPtr, env);
    ASSERT(IS_INT(rsltPtr),"Result should be int");
    ASSERT(INT_VALUE(rsltPtr) == 9,"Result should be 9");



    TEST_SUB_CASE("multiple arg");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(define sum (lambda (a b) (+ a b)))")); fclose(tmpfile);
    eval(objPtr, env);
    objPtr = read(tmpfile = FILE_FROM_STR("(sum 7 2)")); fclose(tmpfile);
    rsltPtr = eval(objPtr, env);
    ASSERT(IS_INT(rsltPtr),"Result should be int");
    ASSERT(INT_VALUE(rsltPtr) == 9,"Result should be 9");



    TEST_SUB_CASE("multiple arg multiple body");
    env = makeEnv(NULL);
    initEnvNatives(env);
    objPtr = read(tmpfile = FILE_FROM_STR("(define sum (lambda (a b) (- a b) (+ a b)))")); fclose(tmpfile);
    eval(objPtr, env);
    objPtr = read(tmpfile = FILE_FROM_STR("(sum 7 2)")); fclose(tmpfile);
    rsltPtr = eval(objPtr, env);
    ASSERT(IS_INT(rsltPtr),"Result should be int");
    ASSERT(INT_VALUE(rsltPtr) == 9,"Result should be 9");


    /* Stack required for PUSH and POP */
    destroyStack();
}
void testComments() {
    TEST_GROUP("Test comments");
    ScmEnvPtr env;
    FILE* tmpfile;

    /* Stack required for PUSH and POP */
    initStack(100, 100);

TEST_CASE("One line comment ';'");

    TEST_SUB_CASE("only comment");
    env = makeEnv(NULL);
    initEnvNatives(env);
    repl(tmpfile = FILE_FROM_STR(";This is a comment (x y z) 1\n"), stderr, env, 0); fclose(tmpfile);


    TEST_SUB_CASE("between expressions");
    env = makeEnv(NULL);
    initEnvNatives(env);
    repl(tmpfile = FILE_FROM_STR("(+ 1 2)\n;comment (/ x y) c\n(+ 9 8)"), stderr, env, 0); fclose(tmpfile);



    TEST_SUB_CASE("inside expressions");
    env = makeEnv(NULL);
    initEnvNatives(env);
    repl(tmpfile = FILE_FROM_STR("(+\n;first arg (a)\n\t1;this is it \n;second arg\n\t2\n;end\n)"), stderr, env, 0); fclose(tmpfile);


    /* Stack required for PUSH and POP */
    destroyStack();

}

void testIntegrity() {
    ScmObjPtr rslt;
    FILE* tmpfile;

TEST_GROUP("Test integrity");
    initStack(100, 100000);
    SCM_GLOBAL_ENV = makeEnv(NULL);
    initEnvNatives(SCM_GLOBAL_ENV);

    evalFile(makeString("init.scm"), SCM_GLOBAL_ENV, REPL_DEFAULT_CONFIG);


TEST_CASE("factorial test");
    repl(tmpfile = FILE_FROM_STR("(! 20)"), stdout, SCM_GLOBAL_ENV, REPL_DEFAULT_CONFIG); fclose(tmpfile);


TEST_CASE("make-adder test (closure)");
    repl(tmpfile = FILE_FROM_STR("(define make-adder\n"
                               "    (lambda (n)\n"
                               "        (lambda (x)\n"
                               "            (+ n x)\n"
                               "        )\n"
                               "    )\n"
                               ")"), stdout, SCM_GLOBAL_ENV, REPL_DEFAULT_CONFIG); fclose(tmpfile);
    repl(tmpfile = FILE_FROM_STR("(define add5 (make-adder 5))"), stdout, SCM_GLOBAL_ENV, REPL_DEFAULT_CONFIG); fclose(tmpfile);
    repl(tmpfile = FILE_FROM_STR("(define add2 (make-adder 2))"), stdout, SCM_GLOBAL_ENV, REPL_DEFAULT_CONFIG); fclose(tmpfile);
    rslt = eval(read(tmpfile = FILE_FROM_STR("(add5 6)")), SCM_GLOBAL_ENV); fclose(tmpfile);
    ASSERT(IS_INT(rslt), "Should be integer");
    ASSERT(INT_VALUE(rslt) == 11, "Should be 11");
    rslt = eval(read(tmpfile = FILE_FROM_STR("(add2 5)")), SCM_GLOBAL_ENV); fclose(tmpfile);
    ASSERT(IS_INT(rslt), "Should be integer");
    ASSERT(INT_VALUE(rslt) == 7, "Should be 7");
    rslt = eval(read(tmpfile = FILE_FROM_STR("(add2 (add5 (add2 3)))")), SCM_GLOBAL_ENV); fclose(tmpfile);
    ASSERT(IS_INT(rslt), "Should be integer");
    ASSERT(INT_VALUE(rslt) == 12, "Should be 12");


TEST_CASE("evaluate lambda inline");
    TEST_SUB_CASE("((lambda () \"OK\"))");
    rslt = eval(read(tmpfile = FILE_FROM_STR("((lambda () \"OK\"))")), SCM_GLOBAL_ENV); fclose(tmpfile);
    ASSERT(IS_STRING(rslt), "Should be integer");
    ASSERT(OBJ_STR_EQ(rslt, "OK"), "Should be \"OK\"");

    TEST_SUB_CASE("((lambda (x y) (+ x y)) 2 5)");
    rslt = eval(read(tmpfile = FILE_FROM_STR("((lambda (x y) (+ x y)) 2 5)")), SCM_GLOBAL_ENV); fclose(tmpfile);
    ASSERT(IS_INT(rslt), "Should be integer");
    ASSERT(INT_VALUE(rslt) == 7, "Should be 7");



    /* Stack required for PUSH and POP */
    destroyStack();
}

void testScm() {
    TEST_GROUP("Test \"test.scm\"");

    initStack(100, 10000);
    SCM_GLOBAL_ENV = makeEnv(NULL);
    initEnvNatives(SCM_GLOBAL_ENV);

    evalFile(makeString("test.scm"), SCM_GLOBAL_ENV, REPL_DEFAULT_CONFIG);
    destroyStack();
}

void runTests() {
    printf("=== TESTS BEGIN ===\n");

    testDefinitions();
    testEnvironment();
    testReader();

    destroyStack();

    testEval();
    testNativeFunctions();
    testNativeSyntax();
    testUserFunctions();
    testComments();
    testIntegrity();
    testScm();

    if (testFailsCounter == 0) {
        printf("=== TESTS END ===\n\n");
    } else {
        printf("=== TESTS FAILED (%d) ===\n\n", testFailsCounter);
        abort();
    }
}