# Declaration of variables
CC = gcc
CC_FLAGS = -std=c99 -Wall -pedantic -D__DEBUG__
 
# File names
EXEC =./bin/scheme
SOURCES = $(wildcard *.c)
OBJECTS = $(SOURCES:.cpp=.o)
 
# Main target
$(EXEC): $(OBJECTS)
	$(CC) $(OBJECTS) -o $(EXEC)
 
# To obtain object files
%.o: %.cpp
	$(CC) -c $(CC_FLAGS) $< -o $@
 
# To remove generated files
clean:
	rm -f $(EXEC) $(OBJECTS)
