
;
; ---- ASSERTION FRAMEWORK ----
;
(define __ASSERT_CNT 0)

(define (ASSERT_EQUAL a b text)
    (if (equal? a b)
        #void
        ((lambda ()
            (set! __ASSERT_CNT (+ 1 __ASSERT_CNT))
            (display "ASSERT_EQUAL failed: " text)
            (newline)
            (display "  Expected: " b)
            (newline)
            (display "  Found: " a)
            (newline)
        ))
    )
)

(define (ASSERT_TRUE predicate text)
    (if (equal? predicate #t)
       #void
       ((lambda ()
           (set! __ASSERT_CNT (+ 1 __ASSERT_CNT))
           (display "ASSERT_TRUE failed: " text)
           (newline)
           (display "  Expected: #t")
           (newline)
           (display "  Found: " predicate)
           (newline)
       ))
   )
)

(define (ASSERT_FALSE predicate text)
    (if (equal? predicate #f)
       #void
       ((lambda ()
           (set! __ASSERT_CNT (+ 1 __ASSERT_CNT))
           (display "ASSERT_FALSE failed: " text)
           (newline)
           (display "  Expected: #f")
           (newline)
           (display "  Found: " predicate)
           (newline)
       ))
   )
)

(define (CHECK_ASSERTS)
    (cond ((> __ASSERT_CNT 0)
        (display ">>> " __ASSERT_CNT " ASSERTIONS FAILED <<<") (newline))))

