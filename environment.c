#include <string.h>
#include "scheme.h"

#define DEBUG_ENV false
#define DEBUG_OBJECTS false

static unsigned int DEFAULT_ENV_SIZE = 2;
static unsigned int INCREASE_FACTOR = 2;

/* TODO: upgrade ENV to use hashing function for key-value storage */

ScmEnvEntryArrayPtr makeEnvEntryArray(unsigned size) {
    DEBUG(DEBUG_OBJECTS, printf("Allocate memory for ENV ENTRY ARRAY of size %d\n", size));
    ScmEnvEntryArrayPtr newEnvEntryArrayPtr = (ScmEnvEntryArrayPtr)scmMalloc(
            sizeof(ScmEnvEntryArray) /* Size of structure ScmEnvEntryArray */
            + (sizeof(EnvEntry) * (size - 1)), /* Size of ScmEnvEntry * count of entries, -1 for the one allocated in the structure ScmEnvEntryArray */
            TAG_ENV_ENTRY_ARRAY);
    newEnvEntryArrayPtr->size = size;
    newEnvEntryArrayPtr->count = 0;
    return newEnvEntryArrayPtr;
}

inline ScmEnvPtr makeEnv(ScmEnvPtr parentEnvPtr) {
    return makeEnvWithSize(parentEnvPtr, DEFAULT_ENV_SIZE);
}

ScmEnvPtr makeEnvWithSize(ScmEnvPtr parentEnvPtr, unsigned defaultSize) {
    DEBUG(DEBUG_OBJECTS, printf("Allocate memory for ENV ENTRY of size %d\n", defaultSize));
    ScmEnvPtr newEnvPtr = (ScmEnvPtr)scmMalloc(sizeof(ScmEnv), TAG_ENV);
    newEnvPtr->parent = parentEnvPtr;
    newEnvPtr->entryArray = NULL;
    TMP_PUSH(newEnvPtr);
    newEnvPtr->entryArray = makeEnvEntryArray(defaultSize);
    TMP_POP();
    return newEnvPtr;
}

static void increaseEnvSize(ScmEnvPtr env) {
    DEBUG(DEBUG_ENV, printEnvStdout(env));

    ScmEnvEntryArrayPtr oldEntryArray = env->entryArray;

    unsigned newEntryArraySize = oldEntryArray->size * INCREASE_FACTOR;
    TMP_PUSH(oldEntryArray);
    env->entryArray = makeEnvEntryArray(newEntryArraySize);
    TMP_POP();


    /* copy entries form old array to the new one */
    for (int i = 0; i < oldEntryArray->size; ++i) {
        env->entryArray->entries[i] = oldEntryArray->entries[i];
    }
    env->entryArray->count = oldEntryArray->count;

    DEBUG(DEBUG_ENV, printEnvStdout(env));
}

static bool _setEnvValue(ScmEnvPtr envPtr, ScmObjPtr key, ScmObjPtr value, bool defineValue, bool reDefineInParent) {
    if (!IS_SYMBOL(key)) {
        errorf("defineEnvValue: key is not a symbol (tag: %d)\n", TAG_OF(key));
    }

    // Increase env size
    if (envPtr->entryArray->count == envPtr->entryArray->size) {
        TMP_PUSH2(key, value);
        increaseEnvSize(envPtr);
        TMP_POP2();
    }

    // Search env for existing key
    for (int i = 0; i < envPtr->entryArray->count; ++i) {
        if (envPtr->entryArray->entries[i].key != NULL && SYMBOLS_EQ(key, envPtr->entryArray->entries[i].key)) {
            envPtr->entryArray->entries[i].value = value;
            return true;
        }
    }

    // Search in parent
    bool success = false;
    if (reDefineInParent && envPtr->parent != NULL) { /* executed only and only when could be defined in parernt */
        success = _setEnvValue(envPtr->parent, key, value, false, reDefineInParent);
    }

    // Add key-value
    if (!success && defineValue) {
        envPtr->entryArray->entries[envPtr->entryArray->count].key = key;
        envPtr->entryArray->entries[envPtr->entryArray->count].value = value;
        envPtr->entryArray->count++;
        return true;
    }
    return success;
}

/**
 * Defines key in environment or redefines in parent.
 *
 * 1) Try to redefine key in provided environemnt.
 * 2) Try to redefine key in parent environemts (recursively).
 * 3) Defines key in current environment.
 */
bool defineEnvValue(ScmEnvPtr envPtr, ScmObjPtr key, ScmObjPtr value) {
    return _setEnvValue(envPtr, key, value,
                        true /* define value if not exist */,
                        true /* indicates if redefine key in parent env if not exist in current env */);
}

/**
 * Defines key in provided environment.
 * Key will be set only and only in provided enviromnment, neverthless key exists in parent environment.
 *
 * 1) Try to redefine key in provided environemnt.
 * 2) Defines key in current environment.
 */
bool letEnvValue(ScmEnvPtr envPtr, ScmObjPtr key, ScmObjPtr value) {
    return _setEnvValue(envPtr, key, value,
                        true /* define value if not exist */,
                        false /* indicates if redefine key in parent env if not exist in current env */);
}

/**
 * Set key in provided environment or parents.
 *
 * 1) Try to redefine key in provided environemnt.
 * 2) Try to redefine key in parent environemts (recursively).
 * 3) Nothing set if key not exist and returned false
 *
 * return: true if key set
 */
bool setEnvValue(ScmEnvPtr envPtr, ScmObjPtr key, ScmObjPtr value) {
    return _setEnvValue(envPtr, key, value,
                        false /* define value if not exist */,
                        true /* indicates if redefine key in parent env if not exist in current env */);
}

ScmObjPtr getEnvValue(ScmEnvPtr envPtr, ScmObjPtr key) {
    if (!IS_SYMBOL(key)) {
        errorf("getEnvValue: key is not a symbol (tag: %d)\n", TAG_OF(key));
    }
    for (int i = 0; i < envPtr->entryArray->count; ++i) {
        ScmObjPtr entryKey = envPtr->entryArray->entries[i].key;
        if (entryKey != NULL && SYMBOLS_EQ(entryKey, key)) {
            return envPtr->entryArray->entries[i].value;
        }
    }
    if (envPtr->parent != NULL) {
        return getEnvValue(envPtr->parent, key);
    }
    return NULL;
}

void printEnvStdout(ScmEnvPtr envPtr) {
    printEnv(envPtr, stdout);
}

void printEnv(ScmEnvPtr envPtr, FILE* out) {
    fprintf(out, "env@%p (%d/%d)\n", (void*) envPtr, envPtr->entryArray->count, envPtr->entryArray->size);
    if (envPtr->parent != NULL) {
        fprintf(out,"-> parent-env@%p\n", (void*) envPtr->parent);
    }
    for(int i = 0; i < envPtr->entryArray->count; i++) {
        fprintf(out, "[%d] '", i);
        print(envPtr->entryArray->entries[i].key, out);
        fprintf(out, "' => ");
        print(envPtr->entryArray->entries[i].value, out);
        fprintf(out, "\n");
    }
}