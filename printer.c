#include "scheme.h"

void printCons(ScmObjPtr consObj, FILE *out) {
    switch (consObj->header.tag) {
        case TAG_NIL:
            fprintf(out, ")");
            break;
        case TAG_CONS:
            print(consObj->cons.car, out);
            if (consObj->cons.cdr->header.tag != TAG_NIL) {
                fprintf(out, " ");
            }
            printCons(consObj->cons.cdr, out);
            break;
        default:
            errorf("Cons cdr can not be of type %d\n", consObj->header.tag);
    }
}

void printLambda(ScmObjPtr objPtr, FILE *out) {
    fprintf(out, "(lambda (");
    if (IS_SYMBOL(objPtr->userFunc.argList)) {
        /* Variable argument list length */
        print(objPtr->userFunc.argList, out);
    } else {
        /* Predefined argument list */
        printCons(objPtr->userFunc.argList, out);
    }
    fprintf(out, " ");
    printCons(objPtr->userFunc.bodyList, out);
}

void printEnvEntryArray(ScmObjPtr objPtr, FILE *out) {
    fprintf(out, "%d { ", objPtr->envEntryArray.count);
    for (int i = 0; i < objPtr->envEntryArray.count; ++i) {
        print(objPtr->envEntryArray.entries[i].key, out);
        fprintf(out, ", ");
    }
    fprintf(out, "}");
}

void print(ScmObjPtr objPtr, FILE *out) {
    switch(objPtr->header.tag) {
        case TAG_INT:
            fprintf(out, "%d", objPtr->integer.value);
            break;
        case TAG_STRING:
            fprintf(out, "\"%s\"", objPtr->string.value);
            break;
        case TAG_SYMBOL:
            fprintf(out, "%s", objPtr->symbol.value);
            break;
        case TAG_FALSE:
            fprintf(out, "#f");
            break;
        case TAG_TRUE:
            fprintf(out, "#t");
            break;
        case TAG_NIL:
            fprintf(out, "'()");
            break;
        case TAG_VOID:
            /* Print nothing, old strategy: fprintf(out, "#void"); */
            return;
        case TAG_EOF:
            fprintf(out, "#eof");
            return;
        case TAG_CONS:
            fprintf(out, "(");
            printCons(objPtr, out);
            break;
        case TAG_NATIVE_FUNC:
            fprintf(out, "<native-function>");
            break;
        case TAG_NATIVE_SYNTAX:
            fprintf(out, "<native-syntax>");
            break;
        case TAG_USER_FUNC:
            printLambda(objPtr, out);
            break;
        case TAG_FREE:
            fprintf(out, "FREE");
            break;
        case TAG_ENV:
            fprintf(out, "env@%p {parent=env@%p", (void*)objPtr, (void*)(objPtr->env.parent));
            if (objPtr->env.entryArray != NULL) {
                fprintf(out, ", count=%u, size=%u}", objPtr->env.entryArray->count, objPtr->env.entryArray->count);
            } else {
                fprintf(out, "} NO INFORMATION ABOUT ENTRY ARRAY (PROBABLY GC RUNS BEFORE ENV ENTRY ARRAY ALLOCATION)");
            }
            break;
        case TAG_ENV_ENTRY_ARRAY:
            printEnvEntryArray(objPtr, out);
            break;
        default:
            errorf("Print for TAG (%d) not implemented yet\n", objPtr->header.tag);
    }
}