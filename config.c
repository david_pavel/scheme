#include "scheme.h"
#include "string.h"
#include <getopt.h>

static char scmLibsInit[] = "init.scm";

ScmConfigPtr defaultScmConfig() {
    ScmConfigPtr config = (ScmConfigPtr)malloc(sizeof(ScmConfig));

    config->heapChunkSize = 1024 * 1024 * 4; /* 4MB */
    config->heapMaxChunksCount = 1024;       /* 4GB total */

    config->stackInitSize =    1000; /*     1,000 objects on stack */
    config->stackMaxSize =  1000000; /* 1,000,000 objects on stack */

    config->verbose = false;
    config->runTest = false;
    config->runBenchmark = false;
    config->resumeOnError = false;

    config->librariesCnt = 1;
    config->libraries = malloc(sizeof(char*) * config->librariesCnt);
    config->libraries[0] = malloc(sizeof(char) * strlen(scmLibsInit) + 1);
    strcpy(config->libraries[0], scmLibsInit);

    config->filesCnt = 0;

    return config;
}

void destroyScmConfig(ScmConfigPtr configPtr) {
    for (int i = 0; i < configPtr->librariesCnt; ++i) {
        free(configPtr->libraries[i]);
    }
    if (configPtr->librariesCnt > 0) {
        free(configPtr->libraries);
    }

    for (int i = 0; i < configPtr->filesCnt; ++i) {
        free(configPtr->files[i]);
    }
    if (configPtr->filesCnt > 0) {
        free(configPtr->files);
    }

    free(configPtr);
}

long parse_long(char* str) {
    char *endp = NULL;
    long size = -1;
    if (!str || ((size = strtol(str, &endp, 10)), (endp && *endp))) {
        fprintf(stderr, "invalid c option %s - expecting a number\n", str ? str : "");
        exit(EXIT_FAILURE);
    };
    return size;
}

void printHelp() {
    printf("Usage: scheme [-options] files...\n");
    printf("             (to execute scheme files)\n");
    printf("       scheme [-options]\n");
    printf("             (to execute scheme interpreter)\n");
    printf("where options include:\n");
    printf("    -h\t\t--help\t\t\t\tPrints help\n");
    printf("    -v\t\t--verbose\t\tTurn on verbose mode\n");
    printf("    -t\t\t--test\t\tRun tests\n");
    printf("    -b\t\t--benchmark\t\t\t\tRun benchmark\n");
    printf("    -e\t\t--exclude-core-library\t\t\t\tDo not load core scheme library\n");
    printf("    -r\t\t--resume-on-error\tDo not exit application on error\n");
    printf("    -c\t\t--heap-chunk-size\tSet size of heap chunk.\n");
    printf("    -m\t\t--heap-chunks-max\tSet maximal number of heap chunks which can be allocated in runtime.\n");
    printf("    -i\t\t--stack-size-min\tSet initial count of objects which can be allocated on the stack\n");
    printf("    -s\t\t--stack-size-max\tSet maximal count of objects which can be allocated on the stack\n");
}

ScmConfigPtr parseScmConfig(int argc, char **argv, ScmConfigPtr defaultConfig) {
    int c;

    for(;;) {
        static struct option long_options[] =
            {
                /* These options set a flag. */
                {"verbose",         no_argument,       0, 'v'},
                {"test",            no_argument,       0, 't'},
                {"benchmark",       no_argument,       0, 'b'},
                {"help",            no_argument,       0, 'h'},
                {"exclude-core-library",            no_argument,       0, 'e'},
                /* These options don’t set a flag.
                   We distinguish them by their indices. */
                {"heap-chunk-size", required_argument, 0, 'c'},
                {"heap-chunks-max", required_argument, 0, 'm'},
                {"stack-size-min",  required_argument, 0, 'i'},
                {"stack-size-max",  required_argument, 0, 's'},
                {"resume-on-error", required_argument, 0, 'r'},
                {0, 0,                                 0, 0}
            };
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long(argc, argv, "evtbhrc:m:i:s:", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1) {
            break;
        }

        switch (c)
        {
            case 0:
                /* If this option set a flag, do nothing else now. */
                if (long_options[option_index].flag != 0)
                    break;
                printf ("option %s", long_options[option_index].name);
                if (optarg)
                    printf (" with arg %s", optarg);
                printf ("\n");
                break;

            case 'v':
                defaultConfig->verbose = true;
                break;

            case 'e':
                /* exclude initial libraries */
                for (int i = 0; i < defaultConfig->librariesCnt; ++i) {
                    free(defaultConfig->libraries[i]);
                }
                free(defaultConfig->libraries);
                defaultConfig->librariesCnt = 0;
                break;

            case 'h':
                printHelp();
                exit(0);

            case 'b':
                defaultConfig->runBenchmark = true;
                break;

            case 't':
                defaultConfig->runTest = true;
                break;

            case 'r':
                defaultConfig->resumeOnError = true;
                break;

            case 'c': {
                long size = parse_long(optarg);
                if (size >= 0) {
                    defaultConfig->heapChunkSize = (unsigned) size;
                }
                break;
            }
            case 'm': {
                long size = parse_long(optarg);
                if (size >= 0) {
                    defaultConfig->heapMaxChunksCount = (unsigned) size;
                }
                break;
            }
            case 'i': {
                long size = parse_long(optarg);
                if (size >= 0) {
                    defaultConfig->stackInitSize = (unsigned) size;
                }
                break;
            }
            case 's': {
                long size = parse_long(optarg);
                if (size >= 0) {
                    defaultConfig->stackMaxSize = (unsigned) size;
                }
                break;
            }
            case '?':
                /* getopt_long already printed an error message. */
                printHelp();
                exit(1);

            default:
                printHelp();
                exit(1);
        }
    }
    /* Print any remaining command line arguments (not options). */
    if (optind < argc) {
        defaultConfig->filesCnt = (unsigned)(argc - optind);
        defaultConfig->files = malloc(sizeof(char*) * defaultConfig->filesCnt);
        int fileIdx = 0;
        while (optind < argc) {
            size_t pathLen = strlen(argv[optind]);
            defaultConfig->files[fileIdx] = malloc(pathLen + 1);
            strcpy(defaultConfig->files[fileIdx], argv[optind]);

            fileIdx++; optind++;
        }
    }

    return defaultConfig;
}

void printConfig(ScmConfigPtr configPtr) {
    FILE *out = stdout;
    fprintf(out, "Scheme Configuration:\n");
    fprintf(out, "\tHeap chunk size:\t"); printHumanReadableSize(configPtr->heapChunkSize, out); fprintf(out, "\n");
    fprintf(out, "\tHeap chunks count:\t%d\n", configPtr->heapMaxChunksCount);
    fprintf(out, "\tHeap max size:\t\t"); printHumanReadableSize((unsigned long)configPtr->heapMaxChunksCount * (unsigned long)configPtr->heapChunkSize, out); fprintf(out, "\n");
    fprintf(out, "\tStack init size:\t%u objects\n", configPtr->stackInitSize);
    fprintf(out, "\tStack max size:\t\t%u objects\n", configPtr->stackMaxSize);
    fprintf(out, "\tRun tests:\t\t%s\n", configPtr->runTest ? "true" : "false");
    fprintf(out, "\tRun benchmark:\t\t%s\n", configPtr->runBenchmark ? "true" : "false");
    fprintf(out, "\tResume on error:\t%s\n", configPtr->resumeOnError ? "true" : "false");
    fprintf(out, "\tLibraries (%u):\t\t", configPtr->librariesCnt);
    bool firstFlag = true;
    for (unsigned i = 0; i < configPtr->librariesCnt; ++i) {
        if (firstFlag) {firstFlag = false;} else {fprintf(out, ", ");}
        fprintf(out, "\"%s\"", configPtr->libraries[i]);
    }
    fprintf(out,"\n");
    fprintf(out, "\tFiles (%u):\t\t", configPtr->filesCnt);
    firstFlag = true;
    for (unsigned i = 0; i < configPtr->filesCnt; ++i) {
        if (firstFlag) {firstFlag = false;} else {fprintf(out, ", ");}
        fprintf(out, "\"%s\"", configPtr->files[i]);
    }
    fprintf(out,"\n\n");

}