#include <string.h>
#include "scheme.h"

#define DEBUG_READER 0

#define NO_UNREAD_CHAR -1
#define END_OF_FILE -1

static int unreadChar = NO_UNREAD_CHAR;
static unsigned int DEFAULT_READ_BUFFER_SIZE = 12;

void unread(int c) {
    if (unreadChar == NO_UNREAD_CHAR) {
        unreadChar = c;
    } else {
        errorf("Char unread double time (last: %c, new: %c)\n", unreadChar, c);
    }
}

int nextChar(FILE *in) {
    int nextChar;
    if (unreadChar != NO_UNREAD_CHAR) {
        nextChar = unreadChar;
        unreadChar = NO_UNREAD_CHAR;
        return nextChar;
    } else {
        nextChar = getc(in);
    }
    DEBUG(DEBUG_READER, printf("Next char: %c (%d)\n", nextChar, nextChar));
    return nextChar;
}

bool isWhiteSpace(int c) {
    return c == ' ' || c =='\t' || c =='\n' || c =='\r';
}

bool isDigit(int c) {
    return (c <= '9' && c >= '0');
}

void skipComment(FILE *in) {
    int c;
    do {
        c = nextChar(in);
    } while (c != '\n' && c != END_OF_FILE);
}

void skipWhiteSpace(FILE *in) {
    int c;

    c = nextChar(in);
    for(;;) {
        if (isWhiteSpace(c)) {
            c = nextChar(in);
        } else if (c == ';') {
            skipComment(in);
            c = nextChar(in);
        } else {
            break;
        }
    }

    unread(c);
}

ScmObjPtr readNumber(FILE *in) {
    int c;
    int number = 0;
    while (isDigit(c = nextChar(in))) {
        number = number * 10 + (c - '0');
    }
    unread(c);

    return makeInteger(number);
}

ScmObjPtr readString(FILE *in) {
    int c;
    unsigned int curPos = 0, oldBufferSize, bufferSize = DEFAULT_READ_BUFFER_SIZE;

    char *oldBuffer, *buffer = (char*)malloc(sizeof(char) * bufferSize);

    for(;;) {
        c = nextChar(in);
        if (c == '"') {
            break;
        }
        if (c == END_OF_FILE) {
            buffer[curPos] = '\0';
            errorf("Can not read string. EOF before string end. Already read: '%s'", buffer);
        }
        if (curPos >= bufferSize - 1) {  /* let last buffer position always free for '\0') */
            oldBufferSize = bufferSize;
            oldBuffer = buffer;
            bufferSize *= 2;
            buffer = (char*)malloc(sizeof(char) * bufferSize);
            strncpy(buffer, oldBuffer, oldBufferSize - 1); /* do not copy last buffer position (it is free for '\0') */
            free(oldBuffer);
            DEBUG(DEBUG_READER, printf("Increase buffer size to %d\n",bufferSize));
        }
        buffer[curPos++] = (char)c;
    }
    buffer[curPos] = '\0';

    ScmObjPtr strObj = makeString(buffer);
    free(buffer);
    return strObj;
}


ScmObjPtr readSymbol(FILE *in) {
    int c;
    unsigned int curPos = 0, oldBufferSize, bufferSize = DEFAULT_READ_BUFFER_SIZE;
    char *oldBuffer, *buffer = (char*)malloc(sizeof(char) * bufferSize);

    for(;;) {
        c = nextChar(in);
        if (isWhiteSpace(c) || c == ')' || c == '(' || c == END_OF_FILE) {
            unread(c);
            break;
        }
        if (curPos >= bufferSize - 1) {  /* let last buffer position always free for '\0') */
            oldBufferSize = bufferSize;
            oldBuffer = buffer;
            bufferSize *= 2;
            buffer = (char*)malloc(sizeof(char) * bufferSize);
            strncpy(buffer, oldBuffer, oldBufferSize - 1); /* do not copy last buffer position (it is free for '\0') */
            free(oldBuffer);
            DEBUG(DEBUG_READER, printf("Increase buffer size to %d\n",bufferSize));
        }
        buffer[curPos++] = (char)c;
    }
    buffer[curPos] = '\0';

    if (buffer[0] == '#') {
        switch (buffer[1]) {
            case 't':
                free(buffer);
                return SCM_TRUE_OBJ;
            case 'f':
                free(buffer);
                return SCM_FALSE_OBJ;
            default:
                if (curPos == 4) {
                    if (strcmp("#nil", buffer) == 0) {
                        free(buffer);
                        return SCM_NIL_OBJ;
                    }
                } else if (curPos == 5) {
                    if (strcmp("#void", buffer) == 0) {
                        free(buffer);
                        return SCM_VOID_OBJ;
                    }
                }
        }
    }
    ScmObjPtr objPtr = (ScmObjPtr)makeSymbol(buffer);
    free(buffer);
    return objPtr;
}

ScmObjPtr readList(FILE *in) {
    int c;
    ScmObjPtr car, cdr, cons;

    skipWhiteSpace(in);
    c = nextChar(in);
    if (c == END_OF_FILE) {
        error("EOF when reading list.");
    }
    if (c == ')') {
        return (ScmObjPtr)SCM_NIL_OBJ;
    }
    unread(c);

    car = read(in);
    TMP_PUSH(car);
    cdr = readList(in);
    TMP_POP();
    cons = makeCons(car, cdr);
    return cons;
}


ScmObjPtr readQuote(FILE *in) {
    ScmObjPtr quotedObj, quotedList, quoteSymbol, rsltObj;

    quotedObj = read(in);
    quotedList = makeCons(quotedObj, SCM_NIL_OBJ);
    TMP_PUSH(quotedList);
    quoteSymbol = makeSymbol("quote");
    TMP_POP();
    rsltObj = makeCons(quoteSymbol, quotedList);

    return rsltObj;
}

ScmObjPtr readQuasiquote(FILE *in) {
    ScmObjPtr quasiquoteObj, quasiquoteList, quasiquoteSymbol, rsltObj;

    quasiquoteObj = read(in);
    quasiquoteList = makeCons(quasiquoteObj, SCM_NIL_OBJ);
    TMP_PUSH(quasiquoteList);
    quasiquoteSymbol = makeSymbol("quasiquote");
    TMP_POP();
    rsltObj = makeCons(quasiquoteSymbol, quasiquoteList);

    return rsltObj;
}

ScmObjPtr readUnquote(FILE *in) {
    ScmObjPtr unquoteObj, unquoteList, unquoteSymbol, rsltObj;

    unquoteObj = read(in);
    unquoteList = makeCons(unquoteObj, SCM_NIL_OBJ);
    TMP_PUSH(unquoteList);
    unquoteSymbol = makeSymbol("unquote");
    TMP_POP();
    rsltObj = makeCons(unquoteSymbol, unquoteList);

    return rsltObj;
}

ScmObjPtr read(FILE *in) {
    int c;
    bool digit;

    skipWhiteSpace(in);
    c = nextChar(in);
    switch(c) {
        case END_OF_FILE:
            return SCM_EOF_OBJ;
        case '(':
            return readList(in);
        case '"':
            return readString(in);
        case '`':
            return readQuasiquote(in);
        case ',':
            return readUnquote(in);
        case '\'':
            return readQuote(in);
        case ')':
        case '.':
            errorf("Unexpected character %c\n",c);
        default:
            digit = isDigit(c);
            unread(c);
            if (digit) {
                return readNumber(in);
            } else {
                return readSymbol(in);
            }
    }
}

