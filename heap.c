#include "scheme.h"
#include <string.h>

/* Heap configuration (#ifdef) */
//#define GC_STRESS_TEST
//#define HEAP_DESTROY_CHECK
#define HEAP_PRINT_CHUNKS
//#define HEAP_PRINT_CELLS

/* Debug configuration */
#define HEAP_DEBUG false
#define HEAP_PRINT_ALLOCATE_CHUNK false

/* Chunks */
static unsigned  CHUNK_SIZE;
static unsigned  MAX_CHUNKS_COUNT;
static POINTER  *chunks;
static unsigned  chunksCount = 0;
static unsigned *chunksAllocatedMemory;

#define CHUNK_START(chunkIndex) (chunks[(chunkIndex)])
#define CHUNK_END(chunkIndex) (chunks[(chunkIndex)] + CHUNK_SIZE)


/* Cells */
#define MIN_CELL_SIZE ((unsigned)sizeof(ScmFree) * 2) /* Size of minimal free cell (containing header only) + at least some data */
#define SIZE_OF_CELL(memory) (((ScmObjPtr)(memory))->header.size)
#define IS_CELL_FREE(memory) (((ScmObjPtr)(memory))->header.tag == TAG_FREE)
#define CELL_MARKED(memory) (((ScmObjPtr)(memory))->header.marked)


/* Current cell & chunk pointer */
static POINTER  currentFreeCellPtr;
static unsigned currentChunkIndex;

/* --------------- */
/* ---- CELLS ---- */
/* --------------- */

static ScmFreePtr setCellAsFree(POINTER cellStart, unsigned cellSize) {
    ScmFreePtr freePtr = ((ScmFreePtr)cellStart);
    freePtr->header.size = cellSize;
    freePtr->header.marked = false;
    freePtr->header.tag = TAG_FREE;
    return freePtr;
}

/* ----------------------- */
/* ---- HEAP ITERATOR ---- */
/* ----------------------- */

HeapIterator* heapIteratorCreate() {
    HeapIterator* iterator = (HeapIterator*)malloc(sizeof(HeapIterator));
    heapIteratorReset(iterator);
    return iterator;
}

void heapIteratorDestroy(HeapIterator* iterator) {
    free(iterator);
}

void heapIteratorReset(HeapIterator* iterator) {
    iterator->currentChunkIndex = 0;
    iterator->currentCell = CHUNK_START(iterator->currentChunkIndex);
    iterator->currentChunkMemory = &(chunksAllocatedMemory[iterator->currentChunkIndex]);
}


bool heapIteratorNext(HeapIterator* iterator) {
    iterator->currentCell += SIZE_OF_CELL(iterator->currentCell);

    if (iterator->currentCell >= CHUNK_END(iterator->currentChunkIndex)) {
        iterator->currentChunkIndex++;
        if (iterator->currentChunkIndex >= chunksCount) {
            iterator->currentCell = NULL;
            iterator->currentChunkMemory = NULL;
            iterator->currentChunkIndex = -1;
            return false;
        }
        iterator->currentCell = CHUNK_START(iterator->currentChunkIndex);
        iterator->currentChunkMemory = &(chunksAllocatedMemory[iterator->currentChunkIndex]);
    }

    return true;
}

/* ---------------- */
/* ---- CHUNKS ---- */
/* ---------------- */

static void allocateChunk() {
    if (chunksCount >= MAX_CHUNKS_COUNT) {
        errorf("allocateChunk: Can not allocate chunk. Max chunks count reached (%d)", chunksCount);
    }

    /* Allocate and init chunk */
    chunks[chunksCount] = (POINTER)malloc(CHUNK_SIZE);
    if (chunks[chunksCount] == NULL) {
        error("failed to allocate memory chunk");
    }
    chunksAllocatedMemory[chunksCount] = 0;

    /* Set whole chunk as free cell */
    setCellAsFree(CHUNK_START(chunksCount), CHUNK_SIZE);

    /* Set next allocation free pointer */
    currentFreeCellPtr = CHUNK_START(chunksCount);
    currentChunkIndex = chunksCount;

    /* Increase chunks count */
    chunksCount++;

    DEBUG(HEAP_PRINT_ALLOCATE_CHUNK, printf("Allocating new chunk (%d/%d)\n", chunksCount, MAX_CHUNKS_COUNT));
}

/* -------------- */
/* ---- HEAP ---- */
/* -------------- */

void resetHeapCurrentFreeCell() {
    currentChunkIndex = 0;
    currentFreeCellPtr = CHUNK_START(currentChunkIndex);
}

void initHeap(unsigned chunkSize, unsigned maxChunksCount) {
    if (chunksCount != 0) {
        error("initHeap: Can not reinitialize heap!");
    }

    /* store configuration */
    MAX_CHUNKS_COUNT = maxChunksCount;
    CHUNK_SIZE = chunkSize;

    /* init variables */
    chunks = (POINTER*)malloc(sizeof(POINTER) * MAX_CHUNKS_COUNT);
    chunksAllocatedMemory = (unsigned*)malloc(sizeof(unsigned) * MAX_CHUNKS_COUNT);


    /* Allocate first chunk */
    allocateChunk();
}

inline unsigned heapTotalAllocatedMemory() {
    unsigned totalAllocatedMemory = 0;
    for (unsigned i = 0; i < chunksCount; ++i) {
        totalAllocatedMemory += chunksAllocatedMemory[i];
    }
    return totalAllocatedMemory;
}

static inline unsigned getTotalFreeMemory() {
    return chunksCount * CHUNK_SIZE - heapTotalAllocatedMemory();
}

void printHeap(FILE *out) {
    fprintf(out,     "================== HEAP DUMP ==================\n");
    fprintf(out,     "Chunks count:\t\t%u\n", chunksCount);
    fprintf(out,     "Allocated memory:\t"); printHumanReadableSize(heapTotalAllocatedMemory(), out); fprintf(out, "\n");
    fprintf(out,     "Free memory:\t\t"); printHumanReadableSize(getTotalFreeMemory(), out); fprintf(out, "\n");

#ifdef HEAP_PRINT_CHUNKS
    for (unsigned chunkIdx = 0; chunkIdx < chunksCount; ++chunkIdx) {
        fprintf(out, "\n------------------- CHUNK %2d -----------------\n", chunkIdx);
        fprintf(out, "Address from:\t%p\nAddress to:\t\t%p\n", CHUNK_START(chunkIdx), CHUNK_END(chunkIdx));
        fprintf(out, "Allocated memory:\t");
        printHumanReadableSize(chunksAllocatedMemory[chunkIdx], out);
        fprintf(out, "\n");
        fprintf(out, "Free memory:\t\t");
        printHumanReadableSize(CHUNK_SIZE - chunksAllocatedMemory[chunkIdx], out);
        fprintf(out, "\n");

        #ifdef HEAP_PRINT_CELLS
            /* Print cells in chunk */
            unsigned counter = 0;
            POINTER curCell = CHUNK_START(chunkIdx);
            while (curCell != CHUNK_END(chunkIdx)) {
                /* Print cell info */
                fprintf(out, "%4u) %s\t", counter++, ((ScmObjPtr)curCell)->header.marked ? " m " : IS_CELL_FREE(curCell) ? "   " : "[X]");
                fprintf(out, "%p\t", (void*)curCell);
                printHumanReadableSize(SIZE_OF_CELL(curCell), out);
                fprintf(out, "\t[%s]\t", TAG_NAME(TAG_OF((ScmObjPtr) curCell)));
                print((ScmObjPtr) curCell, out);
                fprintf(out, "\n");

                curCell += SIZE_OF_CELL(curCell);
            }
        #endif

        fprintf(out, "\n");
    }
#endif

    fprintf(out,     "===============================================\n");
}

void printHeapStdOut() {
    printHeap(stdout);
}

void destroyHeap() {
#ifdef HEAP_DESTROY_CHECK
    /* Check that allocated memory of eache chunk is zero after sweep without marking (this means to clear whole heap) */
    sweep();
    for (int c = 0; c < chunksCount; ++c) {
        if (chunksAllocatedMemory[c] != 0) {
            printStack(stderr);
            printHeap(stderr);
            error("[ASSERTION] More MALLOC then FREEs on the heap (Discovered when destroying the heap)!!!");
        }
    }
#endif

    /* Free allocated chunks */
    for (int c = 0; c < chunksCount; ++c) {
        free(chunks[c]);
    }

    /* Free structures */
    free(chunks);
    free(chunksAllocatedMemory);

    chunksCount = 0;
}

static POINTER allocateMemoryCell(unsigned sizeToAllocate, TAG tag) {
    POINTER  allocatedMemoryPtr = NULL;

    /* Loop for memory in all the chunks */
    unsigned initialChunkIndex = currentChunkIndex;
    do {
        /* Loop through memory in the current chunk */
        POINTER  initialCellPtr = currentFreeCellPtr;
        do {
            /* Search in current chunk */
            if (currentFreeCellPtr >= CHUNK_END(currentChunkIndex)) {
                /* End of chunk - start from the start */
                currentFreeCellPtr = CHUNK_START(currentChunkIndex);
            } else if (IS_CELL_FREE(currentFreeCellPtr)
                       && (SIZE_OF_CELL(currentFreeCellPtr) == sizeToAllocate
                        || SIZE_OF_CELL(currentFreeCellPtr) >= sizeToAllocate + MIN_CELL_SIZE)) {
                /* Allocate memory here */
                if (SIZE_OF_CELL(currentFreeCellPtr) == sizeToAllocate) {
                    /* Fill whole free cell ('size' and 'marked' properties are the same as free cell properties.) */
                    allocatedMemoryPtr = currentFreeCellPtr;
                    TAG_OF(allocatedMemoryPtr) = tag;
                    CELL_MARKED(allocatedMemoryPtr) = false;

                    /* Increase allocated size for the current chunk */
                    DEBUG(HEAP_DEBUG, printf("!!! {+} %u memory cell of tag %s at %p\n", sizeToAllocate, TAG_NAME(tag), allocatedMemoryPtr));
                    chunksAllocatedMemory[currentChunkIndex] += sizeToAllocate;
                } else {
                    unsigned oldSize = SIZE_OF_CELL(currentFreeCellPtr);

                    /* Allocate part of free cell */
                    allocatedMemoryPtr = currentFreeCellPtr;
                    SIZE_OF_CELL(allocatedMemoryPtr) = sizeToAllocate;
                    TAG_OF(allocatedMemoryPtr) = tag;
                    CELL_MARKED(allocatedMemoryPtr) = false;

                    /* Move free cell */
                    currentFreeCellPtr += sizeToAllocate;
                    setCellAsFree(currentFreeCellPtr, oldSize - sizeToAllocate);

                    /* Increase allocated size for the current chunk */
                    DEBUG(HEAP_DEBUG, printf("!!! {+} %u memory cell of tag %s at %p\n", sizeToAllocate, TAG_NAME(tag), allocatedMemoryPtr));
                    chunksAllocatedMemory[currentChunkIndex] += sizeToAllocate;
                }
            } else {
                /* Cell not free, skip it */
                currentFreeCellPtr += SIZE_OF_CELL(currentFreeCellPtr);
            }

        } while(allocatedMemoryPtr == NULL && currentFreeCellPtr != initialCellPtr);

        /* Check memory allocated in current chunk */
        if (allocatedMemoryPtr != NULL) {
            /* Memory allocation was successful in the current chunk */
            break;
        } else {
            /* Try allocation in the next chunk */
            currentChunkIndex = (currentChunkIndex + 1) % chunksCount;
            currentFreeCellPtr = CHUNK_START(currentChunkIndex);
        }

    } while(currentChunkIndex != initialChunkIndex);

    return allocatedMemoryPtr;
}


typedef enum {
    MALLOC_STATE_INIT,
    MALLOC_STATE_AFTER_GC,
    MALLOC_STATE_AFTER_NEW_CHUNK
} MallocState;


static POINTER _scmMalloc(unsigned _sizeToMalloc, TAG tag, MallocState mallocState) {
    if (chunksCount == 0) {error("Heap not initialized!");}

    /* 1) Align size */
    unsigned modulus = _sizeToMalloc % MIN_CELL_SIZE;
    unsigned sizeToMalloc = (modulus == 0)
                   ? _sizeToMalloc
                   : _sizeToMalloc + (MIN_CELL_SIZE - modulus);

#ifdef GC_STRESS_TEST
    DEBUG(HEAP_DEBUG, printf(">>> GC STRESS TEST <<<\n"));
    if (mallocState == MALLOC_STATE_INIT) {
        scmGC();
    }
#endif

    /* 2) Allocate memory cell */
    POINTER freeMemoryCell = allocateMemoryCell(sizeToMalloc, tag);

    /* 3) Not enough memory */
    if (freeMemoryCell == NULL) {
        switch (mallocState) {
            case MALLOC_STATE_INIT:
                /* 3.A) Run GC */
                scmGC();
                freeMemoryCell = _scmMalloc(sizeToMalloc, tag, MALLOC_STATE_AFTER_GC);
                break;
            case MALLOC_STATE_AFTER_GC:
                /* 3.B) Allocate new chunk */
                if (chunksCount >= MAX_CHUNKS_COUNT) {
                    printHeap(stderr);
                    error("ALLOCATION FAILED. NOT ENOUGH MEMORY. CAN NOT ALLOCATE NEW CHUNK.");
                }
                allocateChunk();
                freeMemoryCell = _scmMalloc(sizeToMalloc, tag, MALLOC_STATE_AFTER_NEW_CHUNK);
                break;
            case MALLOC_STATE_AFTER_NEW_CHUNK:
                /* 3.C) Out of memory */
                printHeap(stderr);
                errorf("ALLOCATION FAILED. NOT ENOUGH MEMORY FOR ALLOCATE CELL OF SIZE: %d", sizeToMalloc);
                break; /* Not reachable */
            default:
            errorf("_scmMalloc: Unknown malloc state: %d", mallocState);
        }
    }

    /* Assertion for test purpose */
    if (freeMemoryCell == NULL) { error("_scmMalloc: Returning NULL pointer"); }

    /* 4) return allocated memory */
    return freeMemoryCell;
}

POINTER scmMalloc(unsigned sizeToMalloc, TAG tag) {
    return _scmMalloc(sizeToMalloc, tag, MALLOC_STATE_INIT);
}