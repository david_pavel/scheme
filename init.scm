; --- Math Functions ---
(define sqr (lambda (x) (* x x)))

(define !
    (lambda (n)
        (if (= n 1)
            1
            (* (! (- n 1)) n)
        )
    )
)

(define abs (lambda (x)
    (if (< x 0) (- x) x)))


; --- Base operators ---
(define not
    (lambda (bool)
        (if bool
            #f
            #t)
    )
)
(define <
    (lambda (a b)
        (> b a)
    )
)

(define <=
    (lambda (a b)
        (not (> a b))
    )
)

(define >=
    (lambda (a b)
        (not (< a b))
    )
)

(define (zero? number)
    (= number 0))

(define (negative? number)
    (< 0 number))

(define (positive? number)
    (not (negative? number)))

(define (even? number)
    (= (mod number 2) 0))

(define (odd? number)
    (not (even? number)))

; --- Car, Cdr ---
(define (caar l) (car (car l)))
(define (cadr l) (car (cdr l)))
(define (cdar l) (cdr (car l)))
(define (cddr l) (cdr (cdr l)))

(define (caddr l) (cadr (cdr l)))
(define (cadddr l) (caddr (cdr l)))

(define (first l) (car l))
(define (rest l) (cdr l))
(define (last l)
    (cond ((nil? l)
                #nil) ; no item in list
          ((nil? (cdr l))
                (car l)) ; last item
          (else
                (last (cdr l)))))


; --- List Functions ---

(define length
    (lambda (list)
        ; helper function
        (define list-len-helper
            (lambda (list len)
                (if (nil? list)
                    len
                    (list-len-helper (cdr list) (+ 1 len))
                )
            )
        )
        ; evaluate
        (list-len-helper list 0)
    )
)

(define append
    (lambda (l_first l_second)
        (if (nil? l_second)
            l_first
            (if (nil? l_first)
                l_second
                (cons
                    (car l_first)
                    (append (cdr l_first) l_second)
                )
            )
        )
    )
)

(define (remove list item)
  (if (nil? list)
      #nil
      (if (= (car list) item)
          (cdr list)
          (cons (car list) (remove (cdr list) item)))))

(define (smallest list item)
  (if (nil? list)
      item
      (if (< (car list) item)
          (smallest (cdr list) (car list))
          (smallest (cdr list) item))))

(define seq
    (lambda (from to)
        (if (> from to)
            #nil
            (cons
                from
                (seq (+ from 1) to)
            )
        )
    )
)

(define map
    (lambda (func list)
        (if (nil? list)
            #nil
            (cons
                (func (car list))
                (map func (cdr list)))
        )
    )
)

(define apply (lambda (func list)
    (define _apply (lambda (func accumulator list)
        (if (nil? list)
            accumulator
        ;else
            (_apply
                func
                (func accumulator (car list))
                (cdr list)
            )
        )
    ))
    (_apply func (car list) (cdr list))
))

(define (filter func lst)
    (cond ((nil? lst)
                #nil)
          ((func (car lst))
                (cons (car lst) (filter func (cdr lst))))
          (else
                (filter func (cdr lst)))
    )
)

(define (all func lst)
    (cond ((nil? lst)
                #t)
          ((not (func (car lst)))
                #f)
          (else
                (all func (cdr lst)))))

(define (any func lst)
    (cond ((nil? lst)
                #f)
          ((func (car lst))
                #t)
          (else
                (any func (cdr lst)))))

(define (random-list modulus length)
    (if (< length 1)
        #nil
        (cons (random modulus) (random-list modulus (- length 1)))))

(define list-ref
    (lambda (lst place)
      (if (nil? lst)
          #nil
          (if (= place 0)
              (car lst)
              (list-ref (cdr lst) (- place 1))))))

; ----- Sorting algorithms -----

(define (quick-sort l)
  (if (nil? l)
      #nil
      (append (quick-sort (filter (lambda (x) (> (car l) x)) (cdr l)))
            (append
              (list (car l))
              (quick-sort (filter (lambda (x) (not (> (car l) x))) (cdr l)))))))

(define (select-sort l)
    (if (nil? l)
        #nil
        (cons
            (smallest (cdr l) (car l))
            (select-sort
                (remove
                    l
                    (smallest (cdr l) (car l)))))))

(define (btree-sort l)
    ;; Create leaf node
    (define (create-leaf item)
        (list item #nil #nil))


    ;; insert item to the tree
    (define (btree-insert item tree)
        (if (nil? tree)
            ; insert here
            (create-leaf item)
            (if (> (car tree) item)
                ; insert left
                (list
                    (car tree)
                    (btree-insert item (car (cdr tree)))
                    (car (cdr (cdr tree))))
                ; insert right
                (list
                    (car tree)
                    (car (cdr tree))
                    (btree-insert item (car (cdr (cdr tree)))))
            )
        )
    )

    ;; Create binary tree from list
    (define (create-btree l)
        (if (= (length l) 1)
            (create-leaf (car l))
            (btree-insert
                (car l)
                (create-btree (cdr l)))))

    ;; Convert tree to list
    (define (tree->list tree)
        (if (nil? tree)
            tree
            (append
                (tree->list (car (cdr tree))) ; left node
                (cons
                    (car tree) ; self node value
                    (tree->list (car (cdr (cdr tree)))))
            )
        )
    )


    ;; Process binary tree sort
    (if (nil? l)
        #nil
        (tree->list (create-btree l))
))

;
; ---- LOGICAL OPERATORS ----
;

(define (_and2 a b)
    (cond ((equal? a #f) #f)
          ((equal? b #f) #f)
          (else #t)))

(define and (lambda lst
    (if (nil? lst)
        (error "and: require one argument at least")
        (apply _and2 lst))))

(define (_or2 a b)
    (cond ((equal? a #t) #t)
          ((equal? b #t) #t)
          (else #f)))

(define or (lambda lst
    (if (nil? lst)
        (error "or: require one argument at least")
        (apply _or2 lst))))
