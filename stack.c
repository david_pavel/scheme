#include "scheme.h"
/* string.h for memcpy */
#include "string.h"

static unsigned maxStackSize, currentStackSize = 10000;

static ScmObjPtr *stack;
static unsigned int SP = 0;

ScmObjPtr* getStack(unsigned int *stackItemsCount) {
    *stackItemsCount = SP;
    return stack;
}
void printStack(FILE *out) {
    fprintf(out,     "================== STACK DUMP ==================\n");
    fprintf(out,     "Objects count:\t%u/%u\n", SP, maxStackSize);
    for (int i = 0; i < SP; ++i) {
        fprintf(out, "[%3d]\t%s\t", i, TAG_NAME(TAG_OF(stack[i])));
        print(stack[i], out);
        fprintf(out, "\n");
    }
    fprintf(out,     "================================================\n");

}

void initStack(unsigned minStackSize, unsigned _maxStackSize) {
    currentStackSize = minStackSize;
    maxStackSize = _maxStackSize;

    stack = (ScmObjPtr*)malloc(sizeof(ScmObjPtr) * currentStackSize);
    SP = 0;
}

static void increaseStackSize() {
    unsigned oldStackSize = currentStackSize;
    ScmObjPtr *oldStack = stack;

    /* Compute new stack size */
    currentStackSize = (unsigned)(currentStackSize * (double) STACK_INCREASE_FACTOR);
    if (currentStackSize > maxStackSize) {
        currentStackSize = maxStackSize;
    }

    /* Allocate new stack */
    stack = (ScmObjPtr*)malloc(sizeof(ScmObjPtr) * currentStackSize);
    memcpy(stack, oldStack, sizeof(ScmObjPtr) * oldStackSize);

    /* Clear old stack */
    free(oldStack);
}

void destroyStack() {
    if (SP != 0) {
        printStack(stderr);
        error("destroyStack: Stack not empty when destroying");
    }
    free(stack);
}

void resetStack() {
    SP = 0;
}

inline void SP_DEC(int decreaseSize) {
    SP -= decreaseSize;
    if (SP < 0) {
        printStack(stderr);
        resetStack();
        error("SP_DEC: Stack underflow");
    }
}

inline ScmObjPtr POP() {
    if (SP <= 0) {
        printStack(stderr);
        resetStack();
        error("POP: Stack underflow");
    }
    return stack[--SP];
}

inline int SP_GET() {
    return SP;
}

inline void SP_SET_AT(int positionAgainstSP, ScmObjPtr objPtr) {
    int position = SP + positionAgainstSP;
    if (position < 0) {
        printStack(stderr);
        resetStack();
        error("SP_SET_AT: Stack underflow");
    }
    if (position >= currentStackSize) {
        resetStack();
        error("SP_SET_AT: Stack overflow");
    }
    stack[position] = objPtr;
}

inline ScmObjPtr SP_GET_AT(int positionAgainstSP) {
    int position = SP + positionAgainstSP;
    if (position < 0) {
        printStack(stderr);
        resetStack();
        error("SP_GET_AT: Stack underflow");
    }
    if (position >= currentStackSize) {
        resetStack();
        error("SP_GET_AT: Stack overflow");
    }
    return stack[position];
}

inline ScmObjPtr PUSH(ScmObjPtr objPtr) {
    if (SP >= currentStackSize) {
        increaseStackSize();
        if (SP >= currentStackSize) {
            printStack(stderr);
            resetStack();
            error("PUSH: Stack overflow");
        }
    }
    stack[SP++] = objPtr;
    return objPtr;
}
