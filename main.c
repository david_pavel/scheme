#include "scheme.h"

int main(int argc, char **argv) {

    ScmConfigPtr scmConfig = parseScmConfig(argc, argv, defaultScmConfig());

    initScm(scmConfig);

    /* Run scheme */
    if (scmConfig->filesCnt == 0) {
        /* Mode REPL */
        repl(stdin, stdout, SCM_GLOBAL_ENV, REPL_CONFIG_PRINT_RESULT | REPL_CONFIG_SHOW_PROMPT | REPL_CONFIG_SETJMP);
    } else {
        /* Mode file interpreter */
        for (int fileIndex = 0; fileIndex < scmConfig->filesCnt; ++fileIndex) {
            file(scmConfig->files[fileIndex], SCM_GLOBAL_ENV, REPL_CONFIG_PRINT_RESULT);
        }
    }

    destroyScm();

    destroyScmConfig(scmConfig);

    return SCM_EXIT_CODE;
}