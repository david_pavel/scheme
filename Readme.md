# Scheme implementation
Implementation of `Scheme` interpreter in `C`.

Version 1.1

## How to run scheme?

Install using ```make``` command.
 
Execute ```./bin/scheme -h``` to view scheme binary usage.

## Implemented functionality

### Syntax
- +, -, *, /, mod, %, abs, sqr, sqrt, ! (factorial)
- <, >, <=, >=, =, not, odd?, even?, zero?, negative?, positive? (Integers only)
- nil?
- if, cond, and, or
- define, lambda, set!, let
- comment
- constants: #t, #f, #nil
- cons, car, cdr
- list, length, append, remove, smallest
- seq
- env-print
- load (file)
- display, newline
- quote, eval
- map, apply, filter, all, any
- exit
- equal?
- string?, number?, procedure?, symbol?, boolean?, list?
- rand, random-list
- quick-sort, select-sort, btree-sort
- time

### Interpreter
- Closures
- Heap allocation
- Program arguments parsing (getopt)
- execute source files or run in REPL

### Garbage collector

- Mark and Sweep
- Incremental heap allocation

## Nice to have

### Syntax

- eq?, eqv?
- scheme-report-environment, null-environment,
- case
- set-car!, set-cdr!


### Interpreter

- Colored terminal support (highlighted I/O)
- History listing (arrow up/down)
- Symbol completion (TAB)


## Benchmark
#### Sort 2500 integers in range form 0 to 5000
```
Sorting 2500 numbers:
    quick-sort    0.370447 ms
    btree-sort:   4.216625 ms
    select-sort: 19.552140 ms
```

2016-12-02: Heap implemented
```
Sorting 2500 numbers:
    quick-sort:   0.322522 ms
    btree-sort:   3.649757 ms
    select-sort: 19.813030 ms
Used memory: 101 MB
```


2016-12-02: Environment allocated on heap
```
--- Using malloc ---
Sorting 1500 numbers:
    quick-sort:  0.230390 ms
    btree-sort:  1.511997 ms
    select-sort: 6.758197 ms
Used memory: 37MB

--- Using scheme malloc ---
Sorting 1500 numbers:
    quick-sort:   0.213499 ms
    btree-sort:   2.604995 ms
    select-sort: 27.802499 ms
Used memory: 761 MB

=> Environment takes size of 724 MB
```

2016-12-08: Heap allocation bug fixed
```
Sorting 1500 numbers:
    quick-sort:   0.145985 ms
    btree-sort:   2.262926 ms
    select-sort: 21.898020 ms
```

2017-01-04: GC finished
```
Sorting 1500 numbers:
  quick-sort:   0.264930 ms
  btree-sort:   1.759763 ms
  select-sort: 14.469246 ms
```

