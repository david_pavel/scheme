#include "scheme.h"

#define DEBUG_EVAL false

ScmObjPtr evalSymbol(ScmObjPtr objPtr, ScmEnvPtr env) {
    ScmObjPtr evaluatedObj = getEnvValue(env, objPtr);
    if (evaluatedObj == NULL) {
        printEnv(env, stderr);
        errorf("evalSymbol: Symbol '%s' not found in the environment!", SYM_VALUE(objPtr));
    }
    return evaluatedObj;
}

ScmObjPtr evalNativeSyntax(ScmObjPtr syntaxPtr, ScmObjPtr arguments, ScmEnvPtr env) {
    ScmObjPtr unEvaluatedArg;
    int argsCount = 0;

TMP_PUSH2(syntaxPtr, arguments);
    /* push arguments to the stack */
    for(;;) {
        if (IS_CONS(arguments)) {
            argsCount++;
            unEvaluatedArg = CAR_OF(arguments);

            /* push unevaluated argument to the stack */
            PUSH(unEvaluatedArg);

            /* Move next argument */
            arguments = CDR_OF(arguments);
        } else if (IS_NIL(arguments)) {
            break; // reached the end of an arguments
        }
    }
    /* evaluate native syntax */
    ScmObjPtr rslt = (syntaxPtr->nativeSyntax.func)(argsCount, env);
TMP_POP2();
    return rslt;
}

ScmObjPtr evalNativeFunc(ScmObjPtr funcPtr, ScmObjPtr arguments, ScmEnvPtr env) {
    int argsCount = 0;

TMP_PUSH2(funcPtr, arguments);

    /* Evaluate function arguments */
    for(;;) {
        if (IS_CONS(arguments)) {
            argsCount++;

            /* evaluate argument */
            ScmObjPtr evaluatedArg = eval(CAR_OF(arguments), env);

            /* push evaluated function argument */
            PUSH(evaluatedArg);

            /* move next argument */
            arguments = CDR_OF(arguments);
        } else if (IS_NIL(arguments)) {
            break; // reached the end of an arguments
        } else {
            error("Eval native func fail. Arguments list is not a list!");
        }
    }

    /* Call native function with evaluated arguments */
    ScmObjPtr rsltObj = (funcPtr->nativeFunc.func)(argsCount);
TMP_POP2();
    return rsltObj;
}

ScmObjPtr evalUserFunc(ScmObjPtr funcPtr, ScmObjPtr arguments, ScmEnvPtr env) {
    ScmObjPtr formalArgList, bodyList, rsltObj;
    ScmEnvPtr funcEnv, funcParentEnv;
    int counter;

TMP_PUSH2(funcPtr, arguments);

    formalArgList = funcPtr->userFunc.argList;
    bodyList = funcPtr->userFunc.bodyList;
    funcParentEnv = funcPtr->userFunc.parentEnv;

    /* create function environment */
    funcEnv = makeEnv(funcParentEnv); /* TODO: set env default size same as arguments count */

TMP_PUSH(funcEnv);

    /* fill environment with arguments */
    if (IS_SYMBOL(formalArgList)) {
        /* Variable argument list length */
        // TODO eval arguments
        letEnvValue(funcEnv, formalArgList, arguments);
    } else {
        /* Predefined argument list */
        for (;;) {
            /* Validate argument against formalArgList */
            if (IS_NIL(arguments) && IS_NIL(formalArgList)) {
                /* arguments successfully read*/
                break;
            } else if (!IS_NIL(arguments) && IS_NIL(formalArgList)) {
                errora("Eval user func error. Provided more arguments then required by function ", funcPtr);
            } else if (IS_NIL(arguments) && !IS_NIL(formalArgList)) {
                errora("Eval user func error. Required more arguments then provided by function ", funcPtr);
            }

//            /* check argument type is valid*/
//            if (!IS_SYMBOL(CAR_OF(formalArgList))) {
//                errora("Eval user func error. Formal argument should be SYMBOL. But found ", CAR_OF(formalArgList));
//            }

            /* evaluate argument */
            ScmObjPtr evaluatedArg = eval(CAR_OF(arguments), env);

            /* set argument in the environment */
            letEnvValue(funcEnv, CAR_OF(formalArgList), evaluatedArg);
            DEBUG(DEBUG_EVAL, printf("DEBUG arg set to env: '"));
            DEBUG(DEBUG_EVAL, print(CAR_OF(formalArgList), stdout));
            DEBUG(DEBUG_EVAL, printf("'\n"));
            DEBUG(DEBUG_EVAL, printEnvStdout(funcEnv));

            /* move to the next arguments */
            arguments = CDR_OF(arguments);
            formalArgList = CDR_OF(formalArgList);
        }
    }

    /* eval body list */
    counter = 0;
    rsltObj = SCM_VOID_OBJ;
    for(;;) {
        if (IS_CONS(bodyList)) {
            /* eval body */
            rsltObj = eval(CAR_OF(bodyList), funcEnv);

            counter++;
            bodyList = CDR_OF(bodyList);
        } else if (IS_NIL(bodyList)) {
            /* reached the end of an bodyList */
            break;
        } else {
            error("Eval user func fail. Body list is not a list!");
        }
    }
    if (counter == 0) {
        error("Eval user func fail. Body list should not be empty!");
    }

TMP_POP(/* funcEnv */);
TMP_POP2();

    return rsltObj;
}

ScmObjPtr evalCons(ScmObjPtr consObjPtr, ScmEnvPtr env) {
    ScmObjPtr functionObjPtr, car, arguments;

    car = CAR_OF(consObjPtr);
    arguments = CDR_OF(consObjPtr);

    TMP_PUSH(consObjPtr);
    functionObjPtr = eval(car, env);
    TMP_POP();

    switch (TAG_OF(functionObjPtr)) {
        case TAG_NATIVE_FUNC:
            return evalNativeFunc(functionObjPtr, arguments, env);
        case TAG_NATIVE_SYNTAX:
            return evalNativeSyntax(functionObjPtr, arguments, env);
        case TAG_USER_FUNC:
            return evalUserFunc(functionObjPtr, arguments, env);
        default:
            errorf("evalCons: unsupported car tag: '%s'", TAG_NAME_OF(functionObjPtr));
            return NULL; /* make compiler happy */
    }
}

ScmObjPtr evalFile(ScmObjPtr objPtr, ScmEnvPtr env, REPL_CONFIG replConfig) {
    FILE* file = fopen(STR_VALUE(objPtr), "r");
    if (file == NULL) {
        errorf("Eval of file '%s' file failed. File can not be opened.", STR_VALUE(objPtr));
    }
    repl(file, stdout, env, replConfig);

    fclose(file);

    return SCM_VOID_OBJ;
}

ScmObjPtr eval(ScmObjPtr objPtr, ScmEnvPtr env) {
    ScmObjPtr rsltObj;
TMP_PUSH(objPtr);
    switch (TAG_OF(objPtr)) {
        case TAG_INT:
        case TAG_NIL:
        case TAG_FALSE:
        case TAG_TRUE:
        case TAG_STRING:
        case TAG_VOID:
        case TAG_EOF:
            rsltObj = objPtr;
            break;
        case TAG_SYMBOL:
            rsltObj =  evalSymbol(objPtr, env);
            break;
        case TAG_CONS:
            rsltObj = evalCons(objPtr, env);
            break;
        default:
            errorf("Eval of tag %s not supported yet.", TAG_NAME(TAG_OF(objPtr)));
            return NULL; /* make compiler happy */
    }
TMP_POP();
    return rsltObj;
}